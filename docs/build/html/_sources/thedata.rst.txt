########
The Data
########

*******
General
*******
| There are several events emitted with several different data structures.
| Here you can look and see the different data events and the different data structures included.
| All data broadcast is in a JSON format with 1 exception. The string broadcast starts with an 8 character header that represents the length of the string broadcast.
| For most languages, this can be ignored and all data can be received then parsed by using the first ``{`` and the broadcast ends with ``\r``. 
| Using those points as a basis, a substring of the data can be made and then the JSON parsed into a proper JSON object.
| Many languages have implementations for this. Otherwise, you may look at the Third Party list of packages on the connecting page.
| All events have data as the root element and status available as part of the data object.

**********
SDK Events
**********
| Not all raw sdk events are broadcasted from the server. This may change in the future.
| The SCS SDK Events available are:

- SCS Telemetry Init
- SCS Telemetry Shutdown
- SCS Telemetry Configuration
- SCS Telemetry Frame Start
- SCS Telemetry Frame End
- SCS Telemetry Pause

******
Events
******
| Some events share the same data structure, and it's the first one documented below. Others are special events that have a different structure.
| The normal telemetry structure is shared by the following events:

- Intialized
- Telemetry
- Job Started
- Job Continue
- Job Finished
- Paused
- UnPaused
- No Lights
- Telemetry Shut Down
 
| The special events are as follows:

- Speeding
- Collision
- Late

|  
|  

--------------------
Initialization Event
--------------------
This event is fired when the game is first fired up and initialization has been completed. Do not expect to capture this event as this is also when the server is being started. Unless you connect exactly when the game is started and the user clicks OK on the Additional Features screen, this event may not be received.
The status contained in the data object for this event is ``INTIALIZED``.

---------------
Telemetry Event
---------------
This event is fired every second to all connected clients.
The status contained in the data object for this event is ``TELEMETRY``.

-----------------
Job Started Event
-----------------
This event is fired when a job has been started. While a job may be chosen in the menu, this event does not fire until the trailer has been connected.
The status contained in the data object for this event is ``JOB STARTED``.
Available since version: 0.13

------------------
Job Continue Event
------------------
This event is fired when a job is being resumed. When the game is exited cleanly, the current delivery information is saved to a file for the next time the delivery is resumed.
The status contained in the data object for this event is ``JOB CONTINUE``.
Available since version: 0.14

------------------
Job Finished Event
------------------
This event is fired when a job has been completed. This is fired based off of the in-game configuration event and is fired AFTER the user clicks continue on the job results screen.
The status contained in the data object for this event is ``JOB FINISH``.
Available since version: 0.13

-----------------
Game Paused Event
------------------
This event is fired when the game has been paused.
The status contained in the data object for this event is ``PAUSED``.
Available since version: 0.13

-------------------
Game UnPaused Event
-------------------
This event is fired when the game is unpaused.
The status contained in the data object for this event is ``UNPAUSED``.
Available since version: 0.13

---------------
No Lights Event
---------------
This event is fired when drivers should have their lights on but do not. This uses game time to calculate 7AM to 7PM.
The status contained in the data object for this event is ``NO LIGHTS``.
Available since version: 0.13

-------------------------
Telemetry Shut Down Event
-------------------------
This event is fired when the game is closing. This event may not be captured due to the way the SDK works.
The status contained in the data object for this event is ``TELEMETRY SHUT DOWN``.
Available since version: 0.13

---------------
Collision Event
---------------
This event is fired when the minimal detectable damage has been detected, this results in a possible collision. The player may have collided with a pole, AI, another driver, etc...
The status contained in the data object for this event is ``COLLISION``.
Available since version: 0.13

-------------------
Late Delivery Event
-------------------
This event is fired when the delivery is now past the time due for the game.
The status contained in the data object for this event is ``LATE``.
Available since version: 0.13

--------------
Speeding Event
--------------
This event is fired when the truck speed exceeds the speed limit by 7 meters / second(~15mph/~25kmh)
The status contained in the data object for this event is ``SPEEDING``.
Available since version: 0.13

**********
Structures
**********


--------------------------
Normal Telemetry Structure
--------------------------
The normal telemetry structure is as follows::

    ########{
        
    }\r

|  
|  

------------------
Speeding Structure
------------------
The speeding structure is as follows::

    ########{
        data: <Object>
        {
            status: <string>, value: SPEEDING
            offense: <Object>
            {
                reason: <string>, value: speeding
                speed: <float>, truck speed in meters / second
                speedLimit: <float>, speed limit in meters / second, NOTE: 0 means no speed limit
                x: <float>,
                y: <float>,
                z: <float>,
                trailerX: <float>, NOTE: value is 0.00 with no trailer
                trailerY: <float>, NOTE: value is 0.00 with no trailer
                trailerZ: <float>, NOTE: value is 0.00 with no trailer
            }
        }
    }\r

|  
|  

-------------------
Collision Structure
-------------------
The collision structure is as follows::

    ########{
        data: <Object>
        {
            status: <string>, value: COLLISION
            offsense: <Object>
            {
                reason: <string>, value: collision
                speed: <float>, NOTE: truck speed in meters / second
                speedLimit: <float>, NOTE: speed limit in meters / second
                x: <float>,
                y: <float>,
                z: <float>,
                trailerX: <float>, NOTE: value is 0.00 with no trailer
                trailerY: <float>, NOTE: value is 0.00 with no trailer
                trailerZ: <float>, NOTE: value is 0.00 with no trailer
                cabinDamage: <float>, <0,1>
                chassisDamage: <float>, <0,1>
                engineDamage: <float>, <0,1>
                transmissionDamage: <float>, <0,1>
                trailerDamage: <float>, <0,1>, NOTE: value is 0.00 with no trailer
            }
        }
    }\r

|  
|  

--------------
Late Structure
--------------
The late structure is as follows::

    ########{
        data: <Object>
        {
            status: <string>, value: LATE,
            offense: <Object>
            {
                reason: <string>, value: late
                timeDue: <unsigned int>, This number represents the game time that this delivery will be consiedered late. Represented in number of minutes since Jan 1, 2001.
                currentGameTime: <unsigned int>, This number represents the number of minutes elapsed in-game since Jan 1, 2001.
                x: <float>,
                y: <float>,
                z: <float>,
                trailerX: <float>, NOTE: value is 0.00 with no trailer
                trailerY: <float>, NOTE: value is 0.00 with no trailer
                trailerZ: <float>, NOTE: value is 0.00 with no trailer
                fee: <float>, NOTE: not provided by game, just a simple estimation or suggested value in the game's default currency.
            }
        }
    }\r

|  
|  
