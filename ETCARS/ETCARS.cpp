#include "prerequisites.h"
#include "TelemetryServer.h"
#include "NewMessage.h"
//typedef char* (*CPtr) (char*, char*, bool);
//CPtr savePtr = NULL;

int count = 0;
bool newTruck = false;
bool newJob = false;
bool steamAPIResult = false;
float lastSpeed;
float currentDamages[5];
float lastDamages[5];
boost::asio::io_service io_service;

scs_telemetry_register_for_channel_t register_for_channel = NULL;
scs_telemetry_unregister_from_channel_t unregister_from_channel = NULL;
bool canAssignLightsFine = true;
bool canAssignLateFine = true;
bool canAssignSpeedingFine = true;
bool canAssignNoRestFine = true;
bool wasDistanceMet = false;
bool shuttingDown = false;
boost::thread telemBCastThread;
std::thread* bt;

tcp_server* s;
std::vector<boost::shared_ptr<tcp_connection>>::iterator connIT;
bool readyToStart = false;
std::string endOfJSON = "";

CSteamAPIContext SteamContext;
std::string steamUsername1="";
std::string steamID1="";
std::string steamDLCJSON1="{}";
std::string saveDLL = "";

std::vector<TransportLocation> transportLocations;

StorageJob previousJob;
StorageJob shutdownJob;

bool hasLoadedFile = false;
bool jobLocked = true;
API *api;
#ifdef _WIN32
HMODULE hmod; 
#endif






long long lastTelemBCastTime = 0;
long long milliseconds_from_epoch = 0;
boost::posix_time::ptime const time_epoch(boost::gregorian::date(1970, 1, 1));
#pragma region Frame Start - Assign new values to telemetry array
SCSAPI_VOID telemetry_frame_start(const scs_event_t UNUSED(event), const void *const event_info, const scs_context_t UNUSED(context))
{
	const struct scs_telemetry_frame_start_t *const info = static_cast<const scs_telemetry_frame_start_t *>(event_info);

	if (Telemetry::telemetry()->lastTimestamp == static_cast<scs_timestamp_t>(-1)) {
		Telemetry::telemetry()->lastTimestamp = info->paused_simulation_time;
	}

	if (info->flags & SCS_TELEMETRY_FRAME_START_FLAG_timer_restart) {
		Telemetry::telemetry()->lastTimestamp = 0;
	}
	Telemetry::telemetry()->lastTimestamp = info->paused_simulation_time;
	


}
#pragma endregion

#pragma region Frame End - Job Logic!!!
SCSAPI_VOID telemetry_frame_end(const scs_event_t UNUSED(event), const void *const UNUSED(event_info), const scs_context_t UNUSED(context))
{
	
	Telemetry::telemetry()->game->gameTimeTM = { 0 };
	Telemetry::telemetry()->game->gameTimeTM.tm_hour = 0; Telemetry::telemetry()->game->gameTimeTM.tm_sec = 0;
	Telemetry::telemetry()->game->gameTimeTM.tm_year = 100; Telemetry::telemetry()->game->gameTimeTM.tm_mon = 0; Telemetry::telemetry()->game->gameTimeTM.tm_mday = 1;
	Telemetry::telemetry()->game->gameTimeTM.tm_min = Telemetry::telemetry()->game->dateTime;
		if (Telemetry::telemetry()->game->isDriving == true)
		{

			if (Telemetry::telemetry()->job->onJob)
			{
				if (newJob == true && Telemetry::telemetry()->truck->trailerConnected != false && shutdownJob.configLoaded != false && hasLoadedFile != false)
				{
#pragma region Setup New Job

					canAssignLightsFine = true;
					canAssignSpeedingFine = true;
					canAssignNoRestFine = true;

					if (Job::Current()->loadedFromSave == false)
					{
						Job::Current(true);
						Job::Current()->status = 0;


						Job::Current()->timeDue = Telemetry::telemetry()->job->deliveryTime;

						Job::Current()->odometer = Telemetry::telemetry()->truck->odometer;
						//Job::Current()->distanceDriven = 0.00;
						Job::Current()->fuel = Telemetry::telemetry()->truck->fuel->currentLitres;

						Job::Current()->cabinDamage = Telemetry::telemetry()->truck->damages->cabin;
						Job::Current()->chassisDamage = Telemetry::telemetry()->truck->damages->chassis;
						Job::Current()->engineDamage = Telemetry::telemetry()->truck->damages->engine;
						Job::Current()->transmissionDamage = Telemetry::telemetry()->truck->damages->transmission;
						Job::Current()->wheelDamage = Telemetry::telemetry()->truck->damages->wheels;
						Job::Current()->trailerDamage = Telemetry::telemetry()->job->damage;
						Job::Current()->sourceCity = Telemetry::telemetry()->job->sourceCity.c_str();
						Job::Current()->sourceCityID = Telemetry::telemetry()->job->sourceCityID.c_str();
						Job::Current()->destinationCity = Telemetry::telemetry()->job->destinationCity.c_str();
						Job::Current()->destinationCityID = Telemetry::telemetry()->job->destinationCityID.c_str();
						Job::Current()->cargo = Telemetry::telemetry()->job->cargo.c_str();
						Job::Current()->cargoID = Telemetry::telemetry()->job->cargoID.c_str();
						Job::Current()->truckMake = Telemetry::telemetry()->truck->make.c_str();
						Job::Current()->truckMakeID = Telemetry::telemetry()->truck->makeID.c_str();
						Job::Current()->truckModel = Telemetry::telemetry()->truck->model.c_str();
						Job::Current()->truckModelID = Telemetry::telemetry()->truck->modelID.c_str();
						Job::Current()->trailerMass = Telemetry::telemetry()->job->mass;
						Job::Current()->income = Telemetry::telemetry()->job->income;
						Job::Current()->sourceCompany = Telemetry::telemetry()->job->sourceCompany.c_str();
						Job::Current()->sourceCompanyID = Telemetry::telemetry()->job->sourceCompanyID.c_str();
						Job::Current()->destinationCompany = Telemetry::telemetry()->job->destinationCompany.c_str();
						Job::Current()->destinationCompanyID = Telemetry::telemetry()->job->destinationCompanyID.c_str();
						Telemetry::telemetry()->navigation->lowestDistance = 0.00;
						Telemetry::telemetry()->navigation->highestDistance = 0.00;
						Job::Current()->navigationDistanceRemaining = Telemetry::telemetry()->navigation->distance;
						Job::Current()->isMultiplayer = Telemetry::telemetry()->game->isMultiplayer;
						Job::Current()->timeStarted = Telemetry::telemetry()->game->dateTime;
						Job::Current()->pickupX = Telemetry::telemetry()->truck->worldPlacement.GetX();
						Job::Current()->pickupY = Telemetry::telemetry()->truck->worldPlacement.GetY();
						Job::Current()->pickupZ = Telemetry::telemetry()->truck->worldPlacement.GetZ();
						Job::Current()->trailerPickupX = Telemetry::telemetry()->truck->trailerWorldPlacement.GetX();
						Job::Current()->trailerPickupY = Telemetry::telemetry()->truck->trailerWorldPlacement.GetY();
						Job::Current()->trailerPickupZ = Telemetry::telemetry()->truck->trailerWorldPlacement.GetZ();
						Job::Current()->startCabinDamage = Telemetry::telemetry()->truck->damages->cabin;
						Job::Current()->startChassisDamage = Telemetry::telemetry()->truck->damages->chassis;
						Job::Current()->startEngineDamage = Telemetry::telemetry()->truck->damages->engine;
						Job::Current()->startTransmissionDamage = Telemetry::telemetry()->truck->damages->transmission;
						Job::Current()->startWheelDamage = Telemetry::telemetry()->truck->damages->wheels;
						Job::Current()->startTrailerDamage = Telemetry::telemetry()->job->damage;
						Job::Current()->startOdometer = Telemetry::telemetry()->truck->odometer;
						Job::Current()->distanceDriven = 0.00;
						Job::Current()->realTimeStarted = getEpoch();
						Job::Current()->fuelBurned = 0.00;
						Job::Current()->fuelPurchased = 0.00;
						Job::Current()->totalCabinDamage = Telemetry::telemetry()->truck->damages->cabin;
						Job::Current()->totalChassisDamage = Telemetry::telemetry()->truck->damages->chassis;
						Job::Current()->totalEngineDamage = Telemetry::telemetry()->truck->damages->engine;
						Job::Current()->totalTrailerDamage = Telemetry::telemetry()->job->damage;
						Job::Current()->totalTransmissionDamage = Telemetry::telemetry()->truck->damages->transmission;
						Job::Current()->totalWheelDamage = Telemetry::telemetry()->truck->damages->wheels;
						Telemetry::telemetry()->job->transportLocationLock.lock();
						Telemetry::telemetry()->job->transportLocations.clear();
						Telemetry::telemetry()->job->transportLocationLock.unlock();

						shutdownJob.steamID = Job::Current()->steamID;
						shutdownJob.lastOdometer = Telemetry::telemetry()->truck->odometer;
						shutdownJob.distanceDriven = 0.00;
						shutdownJob.lastFuel = Telemetry::telemetry()->truck->fuel->currentLitres;
						shutdownJob.startCabinDamage = Telemetry::telemetry()->truck->damages->cabin;
						shutdownJob.startChassisDamage = Telemetry::telemetry()->truck->damages->chassis;
						shutdownJob.startEngineDamage = Telemetry::telemetry()->truck->damages->engine;
						shutdownJob.startTransmissionDamage = Telemetry::telemetry()->truck->damages->transmission;
						shutdownJob.startWheelDamage = Telemetry::telemetry()->truck->damages->wheels;
						shutdownJob.startTrailerDamage = Telemetry::telemetry()->job->damage;
						shutdownJob.sourceCityID = Telemetry::telemetry()->job->sourceCityID.c_str();
						shutdownJob.destinationCityID = Telemetry::telemetry()->job->destinationCityID.c_str();
						shutdownJob.cargoID = Telemetry::telemetry()->job->cargoID.c_str();
						shutdownJob.truckMakeID = Telemetry::telemetry()->truck->makeID.c_str();
						shutdownJob.truckModelID = Telemetry::telemetry()->truck->modelID.c_str();
						shutdownJob.trailerMass = Telemetry::telemetry()->job->mass;
						shutdownJob.income = Telemetry::telemetry()->job->income;
						shutdownJob.sourceCompanyID = Telemetry::telemetry()->job->sourceCompanyID.c_str();
						shutdownJob.destinationCompanyID = Telemetry::telemetry()->job->destinationCompanyID.c_str();
						shutdownJob.cargoAccessoryID = Telemetry::telemetry()->trailer->cargoAccessoryId.c_str();

						shutdownJob.realTimeStarted = Job::Current()->realTimeStarted;
						shutdownJob.startTrailerDamage = Job::Current()->startTrailerDamage;
						shutdownJob.startEngineDamage = Job::Current()->startEngineDamage;
						shutdownJob.startTransmissionDamage = Job::Current()->startTransmissionDamage;
						shutdownJob.startCabinDamage = Job::Current()->startCabinDamage;
						shutdownJob.startChassisDamage = Job::Current()->startChassisDamage;
						shutdownJob.startWheelDamage = Job::Current()->startWheelDamage;
						shutdownJob.trailerPickupX = Job::Current()->trailerPickupZ;
						shutdownJob.trailerPickupY = Job::Current()->trailerPickupY;
						shutdownJob.trailerPickupZ = Job::Current()->trailerPickupX;
						shutdownJob.pickupX = Job::Current()->pickupZ;
						shutdownJob.pickupY = Job::Current()->pickupY;
						shutdownJob.pickupZ = Job::Current()->pickupX;
						shutdownJob.startOdometer = Job::Current()->startOdometer;
						shutdownJob.timeStarted = Job::Current()->timeStarted;
						SendStatusMessage("JOB STARTED");
						Debugger::log_line("JOB STARTED FIRED");
					}
					else
					{
						Job::Current()->loadedFromSave = false;
						Job::Current()->status = 1;


						Job::Current()->timeDue = Telemetry::telemetry()->job->deliveryTime;

						Job::Current()->odometer = Telemetry::telemetry()->truck->odometer;
						//Job::Current()->distanceDriven = 0.00;
						Job::Current()->fuel = Telemetry::telemetry()->truck->fuel->currentLitres;

						Job::Current()->cabinDamage = Telemetry::telemetry()->truck->damages->cabin;
						Job::Current()->chassisDamage = Telemetry::telemetry()->truck->damages->chassis;
						Job::Current()->engineDamage = Telemetry::telemetry()->truck->damages->engine;
						Job::Current()->transmissionDamage = Telemetry::telemetry()->truck->damages->transmission;
						Job::Current()->wheelDamage = Telemetry::telemetry()->truck->damages->wheels;
						Job::Current()->trailerDamage = Telemetry::telemetry()->job->damage;
						Job::Current()->sourceCity = Telemetry::telemetry()->job->sourceCity.c_str();
						Job::Current()->sourceCityID = Telemetry::telemetry()->job->sourceCityID.c_str();
						Job::Current()->destinationCity = Telemetry::telemetry()->job->destinationCity.c_str();
						Job::Current()->destinationCityID = Telemetry::telemetry()->job->destinationCityID.c_str();
						Job::Current()->cargo = Telemetry::telemetry()->job->cargo.c_str();
						Job::Current()->cargoID = Telemetry::telemetry()->job->cargoID.c_str();
						Job::Current()->truckMake = Telemetry::telemetry()->truck->make.c_str();
						Job::Current()->truckMakeID = Telemetry::telemetry()->truck->makeID.c_str();
						Job::Current()->truckModel = Telemetry::telemetry()->truck->model.c_str();
						Job::Current()->truckModelID = Telemetry::telemetry()->truck->modelID.c_str();
						Job::Current()->trailerMass = Telemetry::telemetry()->job->mass;
						Job::Current()->income = Telemetry::telemetry()->job->income;
						Job::Current()->sourceCompany = Telemetry::telemetry()->job->sourceCompany.c_str();
						Job::Current()->sourceCompanyID = Telemetry::telemetry()->job->sourceCompanyID.c_str();
						Job::Current()->destinationCompany = Telemetry::telemetry()->job->destinationCompany.c_str();
						Job::Current()->destinationCompanyID = Telemetry::telemetry()->job->destinationCompanyID.c_str();
						Telemetry::telemetry()->navigation->lowestDistance = 0.00;
						Telemetry::telemetry()->navigation->highestDistance = 0.00;
						Job::Current()->navigationDistanceRemaining = Telemetry::telemetry()->navigation->distance;
						Job::Current()->isMultiplayer = Telemetry::telemetry()->game->isMultiplayer;
						SendStatusMessage("JOB CONTINUE");
						Debugger::log_line("JOB CONTINUE FIRED");
						Debugger::log_line(std::to_string(Job::Current()->distanceDriven).c_str());
					}

					
					Job::Current()->jobStartedEventFired = true;
					newJob = false;
					
#pragma endregion
				}
				if (newTruck == true && Telemetry::telemetry()->truck->trailerConnected != false)
				{
					newTruck = false;

				}
				if (Telemetry::telemetry()->job->onJob == true && newTruck == false && newJob == false)
				{
					Job::Current()->status = 1;

#pragma region Late / Time
					if (Telemetry::telemetry()->job->isLate == false && Job::Current()->jobStartedEventFired == true)
					{
						unsigned timeDiff = Telemetry::telemetry()->job->deliveryTime - Telemetry::telemetry()->game->dateTime;
						if (timeDiff <= 0)
						{
							Job::Current()->late = true;
							Telemetry::telemetry()->job->isLate = true;
							if (canAssignLateFine == true)
							{
								canAssignLateFine = false;
								Debugger::log_line(std::to_string(timeDiff).c_str());
								Debugger::log_line(std::to_string(Telemetry::telemetry()->job->deliveryTime).c_str());
								Debugger::log_line(std::to_string(Telemetry::telemetry()->game->dateTime).c_str());
								SendLateFeeMessage(Job::Current()->timeDue, Telemetry::telemetry()->game->dateTime, Telemetry::telemetry()->truck->worldPlacement.GetX(), Telemetry::telemetry()->truck->worldPlacement.GetY(), Telemetry::telemetry()->truck->worldPlacement.GetZ(), Telemetry::telemetry()->truck->trailerWorldPlacement.GetX(), Telemetry::telemetry()->truck->trailerWorldPlacement.GetY(), Telemetry::telemetry()->truck->trailerWorldPlacement.GetZ(), (float)0.10*Telemetry::telemetry()->job->income);
							}
						}
						else
						{
							Job::Current()->late = false;
							Telemetry::telemetry()->job->isLate = false;
						}
						Telemetry::telemetry()->job->timeRemaining = Telemetry::telemetry()->job->deliveryTime - Telemetry::telemetry()->game->dateTime;


						Job::Current()->timeRemaining = Telemetry::telemetry()->job->timeRemaining;
					}
					Job::Current()->realTimeTaken = getEpoch() - Job::Current()->realTimeStarted;
#pragma endregion

#pragma region Fuel
					if (Telemetry::telemetry()->truck->fuel->currentLitres > Job::Current()->fuel && !Telemetry::telemetry()->truck->engineEnabled && Telemetry::telemetry()->truck->speed <= 1)
					{
						Job::Current()->fuelPurchased += (Telemetry::telemetry()->truck->fuel->currentLitres - Job::Current()->fuel);
					}
					else if (Telemetry::telemetry()->truck->fuel->currentLitres < Job::Current()->fuel)
					{
						Job::Current()->fuelBurned += (Job::Current()->fuel - Telemetry::telemetry()->truck->fuel->currentLitres);
					}
					Job::Current()->fuel = Telemetry::telemetry()->truck->fuel->currentLitres;


#pragma endregion

#pragma region Distance
					
					if (Telemetry::telemetry()->truck->odometer > Job::Current()->odometer && Telemetry::telemetry()->truck->trailerConnected != false)
					{
						Job::Current()->distanceDriven += (Telemetry::telemetry()->truck->odometer - Job::Current()->odometer);
					}
					else if (Telemetry::telemetry()->truck->odometer < Job::Current()->odometer)
					{
						Job::Current()->distanceDriven -= (Job::Current()->odometer - Telemetry::telemetry()->truck->odometer);
					}
					Job::Current()->odometer = Telemetry::telemetry()->truck->odometer;
					Job::Current()->navigationDistanceRemaining = Telemetry::telemetry()->navigation->distance;
					if (jobLocked && Telemetry::telemetry()->job->onJob && Telemetry::telemetry()->navigation->distance <= 500 && Telemetry::telemetry()->game->paused == false && Job::Current()->distanceDriven >= 0.5f && Telemetry::telemetry()->truck->trailerConnected && Telemetry::telemetry()->truck->speed >= 2)
					{
						jobLocked = false;
						Debugger::log_line("Distance now 500m or less. Unlocking delivery.");
					}

#pragma endregion

#pragma region Top Speed
					if (Job::Current()->topSpeed < Telemetry::telemetry()->truck->speed)
						Job::Current()->topSpeed = Telemetry::telemetry()->truck->speed;
#pragma endregion

#pragma region Was Speeding
					if (Telemetry::telemetry()->navigation->speedLimit > 0.00)
					{
						if (Telemetry::telemetry()->truck->speed > Telemetry::telemetry()->navigation->speedLimit)
						{
							if (Telemetry::telemetry()->truck->speed - Telemetry::telemetry()->navigation->speedLimit >= 7)
							{

								if (canAssignSpeedingFine == true)
								{
									Job::Current()->wasSpeeding = true;
									Job::Current()->speedingCount++;
									canAssignSpeedingFine = false;
									if (Telemetry::telemetry()->job->onJob)
										SendSpeedMessage(Telemetry::telemetry()->truck->speed, Telemetry::telemetry()->navigation->speedLimit, Telemetry::telemetry()->truck->worldPlacement.GetX(), Telemetry::telemetry()->truck->worldPlacement.GetY(), Telemetry::telemetry()->truck->worldPlacement.GetZ(), Telemetry::telemetry()->truck->trailerWorldPlacement.GetX(), Telemetry::telemetry()->truck->trailerWorldPlacement.GetY(), Telemetry::telemetry()->truck->trailerWorldPlacement.GetZ());
									else
										SendSpeedMessage(Telemetry::telemetry()->truck->speed, Telemetry::telemetry()->navigation->speedLimit, Telemetry::telemetry()->truck->worldPlacement.GetX(), Telemetry::telemetry()->truck->worldPlacement.GetY(), Telemetry::telemetry()->truck->worldPlacement.GetZ());
									//SendStatusMessage("SPEEDING");
								}

							}
							else
							{
								canAssignSpeedingFine = true;
							}
						}
						else
						{
							canAssignSpeedingFine = true;
						}
					}
#pragma endregion	

#pragma region Damages

					if (Telemetry::telemetry()->truck->damages->cabin > Job::Current()->cabinDamage)
						Job::Current()->totalCabinDamage += (Telemetry::telemetry()->truck->damages->cabin - Job::Current()->cabinDamage);
					
					if (Telemetry::telemetry()->truck->damages->chassis > Job::Current()->chassisDamage)
						Job::Current()->totalChassisDamage += (Telemetry::telemetry()->truck->damages->chassis - Job::Current()->chassisDamage);

					if (Telemetry::telemetry()->truck->damages->engine > Job::Current()->engineDamage)
						Job::Current()->totalEngineDamage += (Telemetry::telemetry()->truck->damages->engine - Job::Current()->engineDamage);

					if (Telemetry::telemetry()->truck->damages->transmission > Job::Current()->transmissionDamage)
						Job::Current()->totalTransmissionDamage += (Telemetry::telemetry()->truck->damages->transmission - Job::Current()->transmissionDamage);

					if (Telemetry::telemetry()->truck->damages->wheels > Job::Current()->wheelDamage)
						Job::Current()->totalWheelDamage += (Telemetry::telemetry()->truck->damages->wheels - Job::Current()->wheelDamage);
					
					if (Telemetry::telemetry()->job->damage > Job::Current()->trailerDamage)
						Job::Current()->totalTrailerDamage += (Telemetry::telemetry()->job->damage - Job::Current()->trailerDamage);

					Job::Current()->cabinDamage = Telemetry::telemetry()->truck->damages->cabin;
					Job::Current()->chassisDamage = Telemetry::telemetry()->truck->damages->chassis;
					Job::Current()->engineDamage = Telemetry::telemetry()->truck->damages->engine;
					Job::Current()->transmissionDamage = Telemetry::telemetry()->truck->damages->transmission;
					Job::Current()->wheelDamage = Telemetry::telemetry()->truck->damages->wheels;
					Job::Current()->trailerDamage = Telemetry::telemetry()->job->damage;

#pragma endregion





#pragma region Check the nav distance
					if (Telemetry::telemetry()->truck->trailerConnected && Telemetry::telemetry()->navigation->distance > 0)
					{
						if (Telemetry::telemetry()->navigation->distance < Telemetry::telemetry()->navigation->lowestDistance)
							Telemetry::telemetry()->navigation->lowestDistance = Telemetry::telemetry()->navigation->distance;
						if (Telemetry::telemetry()->navigation->distance > Telemetry::telemetry()->navigation->highestDistance)
							Telemetry::telemetry()->navigation->highestDistance = Telemetry::telemetry()->navigation->distance;
					}
#pragma endregion
					

				}
			}//end of onjob
#pragma region Collisions
			currentDamages[0] = Telemetry::telemetry()->truck->damages->cabin;
			currentDamages[1] = Telemetry::telemetry()->truck->damages->chassis;
			currentDamages[2] = Telemetry::telemetry()->truck->damages->engine;
			currentDamages[3] = Telemetry::telemetry()->truck->damages->transmission;
			currentDamages[4] = Telemetry::telemetry()->job->damage;

			if (changeInDamages(lastDamages, currentDamages) == true)
			{
				Debugger::log_line("[ Telemetry ]  Possible Collision");
				
				//SendStatusMessage("POSSIBLE COLLISION");
				if (Telemetry::telemetry()->job->onJob == true)
				{
					Job::Current()->collisionCount += 1;
					SendCollisionMessage(Telemetry::telemetry()->truck->speed, Telemetry::telemetry()->navigation->speedLimit, Telemetry::telemetry()->truck->worldPlacement.GetX(), Telemetry::telemetry()->truck->worldPlacement.GetY(), Telemetry::telemetry()->truck->worldPlacement.GetZ(), (lastDamages[0] - currentDamages[0]), (lastDamages[1] - currentDamages[1]), (lastDamages[2] - currentDamages[2]), (lastDamages[3] - currentDamages[3]), (lastDamages[4] - currentDamages[4]), Telemetry::telemetry()->truck->trailerWorldPlacement.GetX(), Telemetry::telemetry()->truck->trailerWorldPlacement.GetY(), Telemetry::telemetry()->truck->trailerWorldPlacement.GetZ());
				}
				else
					SendCollisionMessage(Telemetry::telemetry()->truck->speed, Telemetry::telemetry()->navigation->speedLimit, Telemetry::telemetry()->truck->worldPlacement.GetX(), Telemetry::telemetry()->truck->worldPlacement.GetY(), Telemetry::telemetry()->truck->worldPlacement.GetZ(), (lastDamages[0] - currentDamages[0]), (lastDamages[1] - currentDamages[1]), (lastDamages[2] - currentDamages[2]), (lastDamages[3] - currentDamages[3]), (lastDamages[4] - currentDamages[4]));
			}

			lastSpeed = Telemetry::telemetry()->truck->speed;
			lastDamages[0] = Telemetry::telemetry()->truck->damages->cabin;
			lastDamages[1] = Telemetry::telemetry()->truck->damages->chassis;
			lastDamages[2] = Telemetry::telemetry()->truck->damages->engine;
			lastDamages[3] = Telemetry::telemetry()->truck->damages->transmission;
			lastDamages[4] = Telemetry::telemetry()->job->damage;
#pragma endregion

#pragma region Not Resting Checks
			if (Telemetry::telemetry()->game->dateTime - Telemetry::telemetry()->game->nextRestStop >= 60 && Telemetry::telemetry()->game->nextRestStop != 0)//1hr in game time
			{
				//resting fine?
				if (canAssignNoRestFine == true)
				{
					canAssignNoRestFine = false;
					//SendStatusMessage("NOT RESTING");
				}
			}
			else
			{
				canAssignNoRestFine = true;
			}
#pragma endregion

#pragma region No Lights
			if (Telemetry::telemetry()->game->gameTimeTM.tm_hour <= 7 || Telemetry::telemetry()->game->gameTimeTM.tm_hour >= 19)
			{
				if ((Telemetry::telemetry()->game->gameTimeTM.tm_hour == 7 && Telemetry::telemetry()->game->gameTimeTM.tm_min == 0) || (Telemetry::telemetry()->game->gameTimeTM.tm_hour == 19 && Telemetry::telemetry()->game->gameTimeTM.tm_min >= 30) || (Telemetry::telemetry()->game->gameTimeTM.tm_hour <= 6 || Telemetry::telemetry()->game->gameTimeTM.tm_hour >= 20))
				{
					//lights should be on

					if (Telemetry::telemetry()->truck->lights->lowBeam == false && Telemetry::telemetry()->truck->lights->highBeam == false && (Telemetry::telemetry()->truck->speed >= 6.7056 || Telemetry::telemetry()->truck->speed <= -6.7056) == true && canAssignLightsFine == true)
					{
						SendStatusMessage("NO LIGHTS");
						canAssignLightsFine = false;
					}
				}
				else if ((Telemetry::telemetry()->game->gameTimeTM.tm_hour == 7 && Telemetry::telemetry()->game->gameTimeTM.tm_min > 0))
				{
					canAssignLightsFine = true;
				}

			}
			else
			{
				canAssignLightsFine = true;
			}
#pragma endregion

		}




}
#pragma endregion

#pragma region Configuration Event - Job, Truck, and Trailer - Job Logic!!!!!
SCSAPI_VOID telemetry_configuration(const scs_event_t event, const void *const event_info, const scs_context_t UNUSED(context))
{
	Telemetry::telemetry()->game->isDriving = false;
	const struct scs_telemetry_configuration_t *const info = static_cast<const scs_telemetry_configuration_t *>(event_info);



#pragma region Truck Configuration Change
	if (strcmp(info->id, SCS_TELEMETRY_CONFIG_truck) == 0)
	{
		bool hasTruck = false;
		for (const scs_named_value_t *cur = info->attributes; cur->name; ++cur)
		{
			if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_brand_id) == 0)
			{
				telemetry_store_string(nullptr, 0, &cur->value, &Telemetry::telemetry()->truck->makeID);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_brand) == 0)
			{
				telemetry_store_string(nullptr, 0, &cur->value, &Telemetry::telemetry()->truck->make);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_id) == 0)
			{
				telemetry_store_string(nullptr, 0, &cur->value, &Telemetry::telemetry()->truck->modelID);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_name) == 0)
			{
				telemetry_store_string(nullptr, 0, &cur->value, &Telemetry::telemetry()->truck->model);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_fuel_capacity) == 0)
			{
				telemetry_store_float(nullptr, 0, &cur->value, &Telemetry::telemetry()->truck->fuel->capacity);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_fuel_warning_factor) == 0)
			{
				telemetry_store_float(nullptr, 0, &cur->value, &Telemetry::telemetry()->truck->fuel->warningLevel);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_air_pressure_warning) == 0)
			{
				telemetry_store_bool(nullptr, 0, &cur->value, &Telemetry::telemetry()->truck->warnings->airPressure);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_air_pressure_emergency) == 0)
			{
				telemetry_store_bool(nullptr, 0, &cur->value, &Telemetry::telemetry()->truck->warnings->airPressureEmergency);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_oil_pressure_warning) == 0)
			{
				telemetry_store_bool(nullptr, 0, &cur->value, &Telemetry::telemetry()->truck->warnings->oilPressure);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_water_temperature_warning) == 0)
			{
				telemetry_store_bool(nullptr, 0, &cur->value, &Telemetry::telemetry()->truck->warnings->waterTemperature);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_battery_voltage_warning) == 0)
			{
				telemetry_store_bool(nullptr, 0, &cur->value, &Telemetry::telemetry()->truck->warnings->batteryVoltage);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_rpm_limit) == 0)
			{
				telemetry_store_float(nullptr, 0, &cur->value, &Telemetry::telemetry()->truck->maxEngineRPM);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_forward_gear_count) == 0)
			{
				telemetry_store_u32(nullptr, 0, &cur->value, &Telemetry::telemetry()->truck->forwardGearCount);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_reverse_gear_count) == 0)
			{
				telemetry_store_u32(nullptr, 0, &cur->value, &Telemetry::telemetry()->truck->reverseGearCount);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_retarder_step_count) == 0)
			{
				telemetry_store_u32(nullptr, 0, &cur->value, &Telemetry::telemetry()->truck->retarderStepCount);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_differential_ratio) == 0)
			{
				telemetry_store_float(nullptr,0,&cur->value, &Telemetry::telemetry()->truck->differentialRatio);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_hook_position) == 0)
			{
				if(&cur->value.value_fvector)
				{
					//telemetry_store_fvector(nullptr,0,&cur->value, &Telemetry::telemetry()->truck->hookPosition);
					Telemetry::telemetry()->truck->hookPosition.SetX((float)cur->value.value_fvector.x);
					Telemetry::telemetry()->truck->hookPosition.SetY((float)cur->value.value_fvector.y);
					Telemetry::telemetry()->truck->hookPosition.SetZ((float)cur->value.value_fvector.z);
					//Telemetry::telemetry()->truck->hookPosition->SetZ(0.00f);
				}
				else
				{
					Telemetry::telemetry()->truck->hookPosition.SetX(0.00f);
					Telemetry::telemetry()->truck->hookPosition.SetY(0.00f);
					Telemetry::telemetry()->truck->hookPosition.SetZ(0.00f);
				}
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_head_position) == 0)
			{
				if(&cur->value.value_fvector)
									{
					//telemetry_store_fvector(nullptr,0,&cur->value, &Telemetry::telemetry()->truck->hookPosition);
					Telemetry::telemetry()->truck->headPosition.SetX((float)cur->value.value_fvector.x);
					Telemetry::telemetry()->truck->headPosition.SetY((float)cur->value.value_fvector.y);
					Telemetry::telemetry()->truck->headPosition.SetZ((float)cur->value.value_fvector.z);
					//Telemetry::telemetry()->truck->headPosition->SetZ(0.00f);
				}
				else
				{
					Telemetry::telemetry()->truck->headPosition.SetX(0.00f);
					Telemetry::telemetry()->truck->headPosition.SetY(0.00f);
					Telemetry::telemetry()->truck->headPosition.SetZ(0.00f);
				}
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_forward_ratio) == 0)
			{
				Telemetry::telemetry()->truck->ratioMutex.lock();
				if(cur->index != SCS_U32_NIL)
				{
					Telemetry::telemetry()->truck->forwardRatios.push_back((float)cur->value.value_float.value);
				}
				else
				{
					Telemetry::telemetry()->truck->forwardRatios.clear();
				}
				Telemetry::telemetry()->truck->ratioMutex.unlock();
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_reverse_ratio) == 0)
			{
				Telemetry::telemetry()->truck->ratioMutex.lock();
				if(cur->index != SCS_U32_NIL)
				{
					Telemetry::telemetry()->truck->reverseRatios.push_back((float)cur->value.value_float.value);
				}
				else
				{
					Telemetry::telemetry()->truck->reverseRatios.clear();
				}
				Telemetry::telemetry()->truck->ratioMutex.unlock();
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_wheel_count) == 0)
			{

				Telemetry::telemetry()->truck->wheelCountLock.lock();
				while (Telemetry::telemetry()->truck->wheelCount > 0) 
				{
					--Telemetry::telemetry()->truck->wheelCount;
					Telemetry::telemetry()->truck->suspensionDeflections[Telemetry::telemetry()->truck->wheelCount] = 0.00f;
					Telemetry::telemetry()->truck->wheelOnGround[Telemetry::telemetry()->truck->wheelCount] = false;
					Telemetry::telemetry()->truck->wheelSubstance[Telemetry::telemetry()->truck->wheelCount] = 0;
					Telemetry::telemetry()->truck->wheelAngularVelocity[Telemetry::telemetry()->truck->wheelCount] = 0.00f;
					unregister_from_channel(SCS_TELEMETRY_TRUCK_CHANNEL_wheel_susp_deflection, Telemetry::telemetry()->truck->wheelCount, SCS_VALUE_TYPE_float);
					unregister_from_channel(SCS_TELEMETRY_TRUCK_CHANNEL_wheel_on_ground, Telemetry::telemetry()->truck->wheelCount, SCS_VALUE_TYPE_bool);
					unregister_from_channel(SCS_TELEMETRY_TRUCK_CHANNEL_wheel_substance, Telemetry::telemetry()->truck->wheelCount, SCS_VALUE_TYPE_u32);
					unregister_from_channel(SCS_TELEMETRY_TRUCK_CHANNEL_wheel_velocity, Telemetry::telemetry()->truck->wheelCount, SCS_VALUE_TYPE_float);
					unregister_from_channel(SCS_TELEMETRY_TRUCK_CHANNEL_wheel_steering, Telemetry::telemetry()->truck->wheelCount, SCS_VALUE_TYPE_float);
					unregister_from_channel(SCS_TELEMETRY_TRUCK_CHANNEL_wheel_rotation, Telemetry::telemetry()->truck->wheelCount, SCS_VALUE_TYPE_float);
				}


				unsigned int wheel_count = cur->value.value_u32.value;
				if(wheel_count > MAX_WHEEL_COUNT)
				{
					wheel_count = MAX_WHEEL_COUNT;
					Debugger::log_line("Wheel count exceeded 20");
					Debugger::log_line(std::to_string(wheel_count).c_str());
				}
				else
				{
					Debugger::log_line("Wheel Count Initialized");
					Debugger::log_line(std::to_string(wheel_count).c_str());
				}



				while (Telemetry::telemetry()->truck->wheelCount < wheel_count) 
				{

						
					Telemetry::telemetry()->truck->suspensionDeflections[Telemetry::telemetry()->truck->wheelCount] = 0.00f;
					Telemetry::telemetry()->truck->wheelOnGround[Telemetry::telemetry()->truck->wheelCount] = false;
					Telemetry::telemetry()->truck->wheelSubstance[Telemetry::telemetry()->truck->wheelCount] = 0;
					Telemetry::telemetry()->truck->wheelAngularVelocity[Telemetry::telemetry()->truck->wheelCount] = 0.00f;
					register_for_channel(SCS_TELEMETRY_TRUCK_CHANNEL_wheel_susp_deflection, Telemetry::telemetry()->truck->wheelCount, SCS_VALUE_TYPE_float, SCS_TELEMETRY_CHANNEL_FLAG_none, telemetry_store_float, (Telemetry::telemetry()->truck->suspensionDeflections + Telemetry::telemetry()->truck->wheelCount));
					register_for_channel(SCS_TELEMETRY_TRUCK_CHANNEL_wheel_on_ground, Telemetry::telemetry()->truck->wheelCount, SCS_VALUE_TYPE_bool, SCS_TELEMETRY_CHANNEL_FLAG_none, telemetry_store_bool, (Telemetry::telemetry()->truck->wheelOnGround + Telemetry::telemetry()->truck->wheelCount));
					register_for_channel(SCS_TELEMETRY_TRUCK_CHANNEL_wheel_substance, Telemetry::telemetry()->truck->wheelCount, SCS_VALUE_TYPE_u32, SCS_TELEMETRY_CHANNEL_FLAG_none, telemetry_store_u32, (Telemetry::telemetry()->truck->wheelSubstance + Telemetry::telemetry()->truck->wheelCount));
					register_for_channel(SCS_TELEMETRY_TRUCK_CHANNEL_wheel_velocity, Telemetry::telemetry()->truck->wheelCount, SCS_VALUE_TYPE_float, SCS_TELEMETRY_CHANNEL_FLAG_none, telemetry_store_float, (Telemetry::telemetry()->truck->wheelAngularVelocity + Telemetry::telemetry()->truck->wheelCount));
					register_for_channel(SCS_TELEMETRY_TRUCK_CHANNEL_wheel_steering, Telemetry::telemetry()->truck->wheelCount, SCS_VALUE_TYPE_float, SCS_TELEMETRY_CHANNEL_FLAG_none, telemetry_store_float, (Telemetry::telemetry()->truck->wheelSteering + Telemetry::telemetry()->truck->wheelCount));
					register_for_channel(SCS_TELEMETRY_TRUCK_CHANNEL_wheel_rotation, Telemetry::telemetry()->truck->wheelCount, SCS_VALUE_TYPE_float, SCS_TELEMETRY_CHANNEL_FLAG_none, telemetry_store_float, (Telemetry::telemetry()->truck->wheelRotation + Telemetry::telemetry()->truck->wheelCount));
					++Telemetry::telemetry()->truck->wheelCount;

				}
				Telemetry::telemetry()->truck->wheelCountLock.unlock();

			}
			else if(strcmp(cur->name,SCS_TELEMETRY_CONFIG_ATTRIBUTE_wheel_position) == 0)
			{
				Telemetry::telemetry()->truck->wheelCountLock.lock();
				if(cur->index != SCS_U32_NIL)
				{
					Telemetry::telemetry()->truck->wheelPosition[cur->index].SetX((float)cur->value.value_fvector.x);
					Telemetry::telemetry()->truck->wheelPosition[cur->index].SetY((float)cur->value.value_fvector.y);
					Telemetry::telemetry()->truck->wheelPosition[cur->index].SetZ((float)cur->value.value_fvector.z);
				}
				Telemetry::telemetry()->truck->wheelCountLock.unlock();
			}
			else if(strcmp(cur->name,SCS_TELEMETRY_CONFIG_ATTRIBUTE_wheel_steerable) == 0)
			{
				Telemetry::telemetry()->truck->wheelCountLock.lock();
				if(cur->index != SCS_U32_NIL)
				{
					if(cur->value.value_bool.value == 0)
						Telemetry::telemetry()->truck->wheelSteerable[cur->index] = false;
					else
						Telemetry::telemetry()->truck->wheelSteerable[cur->index] = true;
				}
				Telemetry::telemetry()->truck->wheelCountLock.unlock();
			}
			else if(strcmp(cur->name,SCS_TELEMETRY_CONFIG_ATTRIBUTE_wheel_simulated) == 0)
			{
				Telemetry::telemetry()->truck->wheelCountLock.lock();
				if(cur->index != SCS_U32_NIL)
				{
					if(cur->value.value_bool.value == 0)
						Telemetry::telemetry()->truck->wheelSimulated[cur->index] = false;
					else
						Telemetry::telemetry()->truck->wheelSimulated[cur->index] = true;
				}
				Telemetry::telemetry()->truck->wheelCountLock.unlock();
			}
			else if(strcmp(cur->name,SCS_TELEMETRY_CONFIG_ATTRIBUTE_wheel_powered) == 0)
			{
				Telemetry::telemetry()->truck->wheelCountLock.lock();
				if(cur->index != SCS_U32_NIL)
				{
					if(cur->value.value_bool.value == 0)
						Telemetry::telemetry()->truck->wheelPowered[cur->index] = false;
					else
						Telemetry::telemetry()->truck->wheelPowered[cur->index] = true;
				}
				Telemetry::telemetry()->truck->wheelCountLock.unlock();
			}
			else if(strcmp(cur->name,SCS_TELEMETRY_CONFIG_ATTRIBUTE_wheel_liftable) == 0)
			{
				Telemetry::telemetry()->truck->wheelCountLock.lock();
				if(cur->index != SCS_U32_NIL)
				{
					if(cur->value.value_bool.value == 0)
						Telemetry::telemetry()->truck->wheelLiftable[cur->index] = false;
					else
						Telemetry::telemetry()->truck->wheelLiftable[cur->index] = true;
				}
				Telemetry::telemetry()->truck->wheelCountLock.unlock();
			}
			else if(strcmp(cur->name,SCS_TELEMETRY_CONFIG_ATTRIBUTE_wheel_radius) == 0)
			{
				Telemetry::telemetry()->truck->wheelCountLock.lock();
				if(cur->index != SCS_U32_NIL)
				{
					Telemetry::telemetry()->truck->wheelRadius[cur->index] = (float)cur->value.value_float.value;
				}
				Telemetry::telemetry()->truck->wheelCountLock.unlock();
			}
			hasTruck = true;
		}
		newTruck = true;
		Telemetry::telemetry()->truck->hasTruck = (hasTruck == false) ? false : true;
		Debugger::log_line("[ Telemetry ] Truck Changed");
		if (!readyToStart)
			readyToStart = true;

		//SendStatusMessage("TRUCK CHANGED");
	}
#pragma endregion
#pragma region Job Configuration Change
	else if (strcmp(info->id, SCS_TELEMETRY_CONFIG_job) == 0)
	{
		bool hasJob = false;
		for (const scs_named_value_t *cur = info->attributes; cur->name; ++cur)
		{
			if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_cargo_id) == 0)
			{
				telemetry_store_string(nullptr, 0, &cur->value, &Telemetry::telemetry()->job->cargoID);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_cargo) == 0)
			{
				telemetry_store_string(nullptr, 0, &cur->value, &Telemetry::telemetry()->job->cargo);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_cargo_mass) == 0)
			{
				telemetry_store_float(nullptr, 0, &cur->value, &Telemetry::telemetry()->job->mass);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_destination_city) == 0)
			{
				telemetry_store_string(nullptr, 0, &cur->value, &Telemetry::telemetry()->job->destinationCity);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_destination_city_id) == 0)
			{
				telemetry_store_string(nullptr, 0, &cur->value, &Telemetry::telemetry()->job->destinationCityID);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_source_city) == 0)
			{
				telemetry_store_string(nullptr, 0, &cur->value, &Telemetry::telemetry()->job->sourceCity);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_source_city_id) == 0)
			{
				telemetry_store_string(nullptr, 0, &cur->value, &Telemetry::telemetry()->job->sourceCityID);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_destination_company_id) == 0)
			{
				telemetry_store_string(nullptr, 0, &cur->value, &Telemetry::telemetry()->job->destinationCompanyID);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_destination_company) == 0)
			{
				telemetry_store_string(nullptr, 0, &cur->value, &Telemetry::telemetry()->job->destinationCompany);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_source_company_id) == 0)
			{
				telemetry_store_string(nullptr, 0, &cur->value, &Telemetry::telemetry()->job->sourceCompanyID);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_source_company) == 0)
			{
				telemetry_store_string(nullptr, 0, &cur->value, &Telemetry::telemetry()->job->sourceCompany);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_income) == 0)
			{
				telemetry_store_u64(nullptr, 0, &cur->value, &Telemetry::telemetry()->job->income);
			}
			else if (strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_delivery_time) == 0)
			{
				telemetry_store_u32(nullptr, 0, &cur->value, &Telemetry::telemetry()->job->deliveryTime);
			}
			hasJob = true;
			jobLocked = true;
		}
		//RESET values if no job...
		if (!hasJob)
		{
			//if we previously had a job, let's mark it completed
			if (Telemetry::telemetry()->job->onJob == true)
			{
				Job::Current()->navigationDistanceRemaining = Telemetry::telemetry()->navigation->distance;
				Job::Current()->endOdometer = Telemetry::telemetry()->truck->odometer;
				

				Job::Current()->wasFinished = !jobLocked;
				Job::Current()->finishTrailerDamage = Telemetry::telemetry()->job->damage;
				Job::Current()->finishCabinDamage = Telemetry::telemetry()->truck->damages->cabin;
				Job::Current()->finishChassisDamage = Telemetry::telemetry()->truck->damages->chassis;
				Job::Current()->finishEngineDamage = Telemetry::telemetry()->truck->damages->engine;
				Job::Current()->finishTransmissionDamage = Telemetry::telemetry()->truck->damages->transmission;
				Job::Current()->finishWheelDamage = Telemetry::telemetry()->truck->damages->wheels;
				Job::Current()->timeDelivered = Telemetry::telemetry()->game->dateTime;
				Job::Current()->deliveryX = Telemetry::telemetry()->truck->worldPlacement.GetX();
				Job::Current()->deliveryY = Telemetry::telemetry()->truck->worldPlacement.GetY();
				Job::Current()->deliveryZ = Telemetry::telemetry()->truck->worldPlacement.GetZ();
				Job::Current()->trailerDeliveryX = Telemetry::telemetry()->truck->trailerWorldPlacement.GetX();
				Job::Current()->trailerDeliveryY = Telemetry::telemetry()->truck->trailerWorldPlacement.GetY();
				Job::Current()->trailerDeliveryZ = Telemetry::telemetry()->truck->trailerWorldPlacement.GetZ();
				Job::Current()->wasTrailerDisconnnected = !Telemetry::telemetry()->truck->trailerConnected;
				Job::Current()->realTimeEnded = getEpoch();
				Job::Current()->realTimeTaken = Job::Current()->realTimeEnded - Job::Current()->realTimeStarted;
				Job::Current()->status = (jobLocked) ? 3 : 2;
				SendStatusMessage("JOB FINISHED");

				shutdownJob.reset();
				previousJob.reset();
				previousJob.steamID = Job::Current()->steamID;
				previousJob.sourceCityID = Job::Current()->sourceCityID;
				previousJob.sourceCompanyID = Job::Current()->sourceCompanyID;
				previousJob.destinationCityID = Job::Current()->destinationCityID;
				previousJob.destinationCompanyID = Job::Current()->destinationCompanyID;
				previousJob.truckMakeID = Job::Current()->truckMakeID;
				previousJob.truckModelID = Job::Current()->truckModelID;
				previousJob.cargoID = Job::Current()->cargoID;
				previousJob.cargoAccessoryID = Telemetry::telemetry()->trailer->cargoAccessoryId.c_str();
				previousJob.income = Job::Current()->income;
				previousJob.trailerMass = Job::Current()->trailerMass;
				doSaveETCARSFile();
				if (!jobLocked)
				{
					UploadCompletedDelivery();
				}
			}
			Telemetry::telemetry()->job->onJob = false;
			
		
			

		}
		else
		{
			Debugger::log_line(std::string("TRAILER CONNECTED STATUS(NEW JOB): " + boolToString(Telemetry::telemetry()->truck->trailerConnected)).c_str());
			Debugger::log_line(std::string("Distance Left(NEW JOB): " + std::to_string(Telemetry::telemetry()->navigation->distance)).c_str());
			Telemetry::telemetry()->job->isLate = (Telemetry::telemetry()->job->deliveryTime - Telemetry::telemetry()->game->dateTime > 0) ? false : true;
			Telemetry::telemetry()->job->timeRemaining = Telemetry::telemetry()->job->deliveryTime - Telemetry::telemetry()->game->dateTime;
			Telemetry::telemetry()->job->transportLocationLock.lock();
			Telemetry::telemetry()->job->transportLocations.clear();
			Telemetry::telemetry()->job->transportLocationLock.unlock();
			wasDistanceMet = false;
			newJob = true;
		}
		//newJob = true; -> NEW JOB FLAG ORIGINALLY SET HERE
		Telemetry::telemetry()->job->onJob = hasJob;
		Job::Current()->onJob = hasJob;
		Debugger::log_line("[ Telemetry ]   Job Changed");
		//SendStatusMessage("JOB CHANGED");

	}
#pragma endregion

#pragma region Trailer Config Event
	else if (strcmp(info->id, SCS_TELEMETRY_CONFIG_trailer) == 0)
	{
		bool hasTrailer = false;
		for (const scs_named_value_t *cur = info->attributes; cur->name; ++cur)
		{
			if(strcmp(cur->name,SCS_TELEMETRY_CONFIG_ATTRIBUTE_hook_position) == 0)
			{
				//telemetry_store_fvector(nullptr, 0, &cur->value, &Telemetry::telemetry()->trailer->hookPosition);
				if(&cur->value.value_fvector)
					{
					//telemetry_store_fvector(nullptr,0,&cur->value, &Telemetry::telemetry()->truck->hookPosition);
					Telemetry::telemetry()->trailer->hookPosition.SetX((float)cur->value.value_fvector.x);
					Telemetry::telemetry()->trailer->hookPosition.SetY((float)cur->value.value_fvector.y);
					Telemetry::telemetry()->truck->headPosition.SetZ((float)cur->value.value_fvector.z);
					//Telemetry::telemetry()->trailer->hookPosition.SetZ(0.00f);
				}
				else
				{
					Telemetry::telemetry()->trailer->hookPosition.SetX(0.00f);
					Telemetry::telemetry()->trailer->hookPosition.SetY(0.00f);
					Telemetry::telemetry()->trailer->hookPosition.SetZ(0.00f);
				}
			}
			else if(strcmp(cur->name,SCS_TELEMETRY_CONFIG_ATTRIBUTE_id) == 0)
			{
				telemetry_store_string(nullptr,0,&cur->value, &Telemetry::telemetry()->trailer->id);
			}
			else if(strcmp(cur->name,SCS_TELEMETRY_CONFIG_ATTRIBUTE_cargo_accessory_id) == 0)
			{
				telemetry_store_string(nullptr,0,&cur->value, &Telemetry::telemetry()->trailer->cargoAccessoryId);
			}
			else if(strcmp(cur->name,SCS_TELEMETRY_CONFIG_ATTRIBUTE_wheel_count)==0)
			{
				Telemetry::telemetry()->trailer->wheelCountLock.lock();
				while (Telemetry::telemetry()->trailer->wheelCount > 0) 
				{
					--Telemetry::telemetry()->trailer->wheelCount;
					Telemetry::telemetry()->trailer->wheelSuspensionDeflections[Telemetry::telemetry()->trailer->wheelCount] = 0.00f;
					Telemetry::telemetry()->trailer->wheelOnGround[Telemetry::telemetry()->trailer->wheelCount] = false;
					Telemetry::telemetry()->trailer->wheelSubstance[Telemetry::telemetry()->trailer->wheelCount] = 0;
					Telemetry::telemetry()->trailer->wheelAngularVelocity[Telemetry::telemetry()->trailer->wheelCount] = 0.00f;
					unregister_from_channel(SCS_TELEMETRY_TRAILER_CHANNEL_wheel_susp_deflection, Telemetry::telemetry()->trailer->wheelCount, SCS_VALUE_TYPE_float);
					unregister_from_channel(SCS_TELEMETRY_TRAILER_CHANNEL_wheel_on_ground, Telemetry::telemetry()->trailer->wheelCount, SCS_VALUE_TYPE_bool);
					unregister_from_channel(SCS_TELEMETRY_TRAILER_CHANNEL_wheel_substance, Telemetry::telemetry()->trailer->wheelCount, SCS_VALUE_TYPE_u32);
					unregister_from_channel(SCS_TELEMETRY_TRAILER_CHANNEL_wheel_velocity, Telemetry::telemetry()->trailer->wheelCount, SCS_VALUE_TYPE_float);
					unregister_from_channel(SCS_TELEMETRY_TRAILER_CHANNEL_wheel_steering, Telemetry::telemetry()->trailer->wheelCount, SCS_VALUE_TYPE_float);
					unregister_from_channel(SCS_TELEMETRY_TRAILER_CHANNEL_wheel_rotation, Telemetry::telemetry()->trailer->wheelCount, SCS_VALUE_TYPE_float);
				}


				unsigned int wheel_count = cur->value.value_u32.value;
				if(wheel_count > MAX_WHEEL_COUNT)
				{
					wheel_count = MAX_WHEEL_COUNT;
					Debugger::log_line("Wheel count(trailer) exceeded 10");
					Debugger::log_line(std::to_string(wheel_count).c_str());
				}
				else
				{
					Debugger::log_line("Wheel Count(trailer) Initialized");
					Debugger::log_line(std::to_string(wheel_count).c_str());
				}



				while (Telemetry::telemetry()->trailer->wheelCount < wheel_count) 
				{

						
					Telemetry::telemetry()->trailer->wheelSuspensionDeflections[Telemetry::telemetry()->trailer->wheelCount] = 0.00f;
					Telemetry::telemetry()->trailer->wheelOnGround[Telemetry::telemetry()->trailer->wheelCount] = false;
					Telemetry::telemetry()->trailer->wheelSubstance[Telemetry::telemetry()->trailer->wheelCount] = 0;
					Telemetry::telemetry()->trailer->wheelAngularVelocity[Telemetry::telemetry()->trailer->wheelCount] = 0.00f;
					register_for_channel(SCS_TELEMETRY_TRAILER_CHANNEL_wheel_susp_deflection, Telemetry::telemetry()->trailer->wheelCount, SCS_VALUE_TYPE_float, SCS_TELEMETRY_CHANNEL_FLAG_none, telemetry_store_float, (Telemetry::telemetry()->trailer->wheelSuspensionDeflections + Telemetry::telemetry()->trailer->wheelCount));
					register_for_channel(SCS_TELEMETRY_TRAILER_CHANNEL_wheel_on_ground, Telemetry::telemetry()->trailer->wheelCount, SCS_VALUE_TYPE_bool, SCS_TELEMETRY_CHANNEL_FLAG_none, telemetry_store_bool, (Telemetry::telemetry()->trailer->wheelOnGround + Telemetry::telemetry()->trailer->wheelCount));
					register_for_channel(SCS_TELEMETRY_TRAILER_CHANNEL_wheel_substance, Telemetry::telemetry()->trailer->wheelCount, SCS_VALUE_TYPE_u32, SCS_TELEMETRY_CHANNEL_FLAG_none, telemetry_store_u32, (Telemetry::telemetry()->trailer->wheelSubstance + Telemetry::telemetry()->trailer->wheelCount));
					register_for_channel(SCS_TELEMETRY_TRAILER_CHANNEL_wheel_velocity, Telemetry::telemetry()->trailer->wheelCount, SCS_VALUE_TYPE_float, SCS_TELEMETRY_CHANNEL_FLAG_none, telemetry_store_float, (Telemetry::telemetry()->trailer->wheelAngularVelocity + Telemetry::telemetry()->trailer->wheelCount));
					register_for_channel(SCS_TELEMETRY_TRAILER_CHANNEL_wheel_steering, Telemetry::telemetry()->trailer->wheelCount, SCS_VALUE_TYPE_float, SCS_TELEMETRY_CHANNEL_FLAG_none, telemetry_store_float, (Telemetry::telemetry()->trailer->wheelSteering + Telemetry::telemetry()->trailer->wheelCount));
					register_for_channel(SCS_TELEMETRY_TRAILER_CHANNEL_wheel_rotation, Telemetry::telemetry()->trailer->wheelCount, SCS_VALUE_TYPE_float, SCS_TELEMETRY_CHANNEL_FLAG_none, telemetry_store_float, (Telemetry::telemetry()->trailer->wheelRotation + Telemetry::telemetry()->trailer->wheelCount));
					++Telemetry::telemetry()->trailer->wheelCount;

				}
				Telemetry::telemetry()->trailer->wheelCountLock.unlock();
			}
			hasTrailer = true;
		}
		if (Telemetry::telemetry()->job->onJob && hasTrailer)
		{
			shutdownJob.reset();
			shutdownJob.steamID = Job::Current()->steamID;
			shutdownJob.sourceCityID = Telemetry::telemetry()->job->sourceCityID.c_str();
			shutdownJob.sourceCompanyID = Telemetry::telemetry()->job->sourceCompanyID.c_str();
			shutdownJob.destinationCityID = Telemetry::telemetry()->job->destinationCityID.c_str();
			shutdownJob.destinationCompanyID = Telemetry::telemetry()->job->destinationCompanyID.c_str();
			shutdownJob.truckMakeID = Telemetry::telemetry()->truck->makeID.c_str();
			shutdownJob.truckModelID = Telemetry::telemetry()->truck->modelID.c_str();
			shutdownJob.cargoAccessoryID = Telemetry::telemetry()->trailer->cargoAccessoryId.c_str();
			shutdownJob.cargoID = Telemetry::telemetry()->job->cargoID.c_str();
			shutdownJob.income = Telemetry::telemetry()->job->income;
			shutdownJob.trailerMass = Telemetry::telemetry()->job->mass;
			shutdownJob.lastFuel = Telemetry::telemetry()->truck->fuel->currentLitres;
			shutdownJob.lastOdometer = Telemetry::telemetry()->truck->odometer;

			doLoadETCARSFile();//LOAD the save 
		}
		shutdownJob.configLoaded = true;
	}
#pragma endregion

#pragma region Substance Config Event
	else if (strcmp(info->id, SCS_TELEMETRY_CONFIG_substances) == 0)
	{
		Telemetry::telemetry()->game->substanceLock.lock();
		Telemetry::telemetry()->game->substances.clear();
		Debugger::log_line("Cleared Substance Array");
		for (const scs_named_value_t *cur = info->attributes; cur->name; ++cur)
		{
			if(cur->index != SCS_U32_NIL)
			{
				Telemetry::telemetry()->game->substances.push_back(static_cast<std::string>(cur->value.value_string.value));
			}
		}
		Telemetry::telemetry()->game->substanceLock.unlock();
	}
#pragma endregion

#pragma region Controls Config Event
	else if (strcmp(info->id, SCS_TELEMETRY_CONFIG_controls) == 0)
	{
		for (const scs_named_value_t *cur = info->attributes; cur->name; ++cur)
		{
			if(strcmp(cur->name, SCS_TELEMETRY_CONFIG_ATTRIBUTE_shifter_type) == 0)
			{
				Telemetry::telemetry()->truck->shifterType = static_cast<std::string>(cur->value.value_string.value);
			}
		}
	}
#pragma endregion
}
#pragma endregion

#pragma region Pause Event
SCSAPI_VOID telemetry_pause(const scs_event_t event, const void *const UNUSED(event_info), const scs_context_t UNUSED(context))
{
	Telemetry::telemetry()->game->paused = (event == SCS_TELEMETRY_EVENT_paused);
	if (Telemetry::telemetry()->game->paused) {
		Debugger::log_line("[Telemetry]   Game Paused");
		Telemetry::telemetry()->game->isDriving = false;
		//sendTelemetry("Paused");
		SendStatusMessage("PAUSED");
		
	}
	else {
		Debugger::log_line("[Telemetry]   Game Unpaused");
		Telemetry::telemetry()->game->isDriving = true;
		//sendTelemetry("UnPaused");
		SendStatusMessage("UNPAUSED");
	}

}
#pragma endregion

#pragma region Conversion Type Functions

#pragma region SCS_VALUE_TYPE_float To float
SCSAPI_VOID telemetry_store_float(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context)
{
	if (!value)
	{
		*static_cast<float *>(context) = 0.00;
		return;
	}
	assert(value);
	assert(value->type == SCS_VALUE_TYPE_float);
	assert(context);
	*static_cast<float *>(context) = value->value_float.value;
}
#pragma endregion

#pragma region SCS_VALUE_TYPE_s32 To int
SCSAPI_VOID telemetry_store_s32(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context)
{
	if (!value) return;
	assert(value);
	assert(value->type == SCS_VALUE_TYPE_s32);
	assert(context);
	*static_cast<int *>(context) = value->value_s32.value;

}
#pragma endregion

#pragma region SCS_VALUE_TYPE_u32 To int
SCSAPI_VOID telemetry_store_u32(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context)
{
	if (!value) return;
	assert(value);
	assert(value->type == SCS_VALUE_TYPE_u32);
	assert(context);
	*static_cast<unsigned int *>(context) = value->value_u32.value;
}
#pragma endregion

#pragma region SCS_VALUE_TYPE_u64 To int
SCSAPI_VOID telemetry_store_u64(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context)
{
	if (!value) return;
	assert(value);
	assert(value->type == SCS_VALUE_TYPE_u64);
	assert(context);
	*static_cast<unsigned int *>(context) = (unsigned int)value->value_u64.value;
}
#pragma endregion

#pragma region SCS_VALUE_TYPE_bool To Bool
SCSAPI_VOID telemetry_store_bool(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context)
{
	if (!context) return;
	if (value)
	{
		if (value->value_bool.value == 0)
			*static_cast<bool*>(context) = false;
		else
			*static_cast<bool*>(context) = true;
	}
	else
		*static_cast<bool*>(context) = false;
}

#pragma endregion


#pragma region SCS_VALUE_TYPE_fplacement to Placement
SCSAPI_VOID telemetry_store_fplacement(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context)
{
	if (!value) return;
	assert(value);
	assert(value->type == SCS_VALUE_TYPE_fplacement);
	assert(context);
	static_cast<Placement *>(context)->SetX((float)value->value_fplacement.position.x);
	static_cast<Placement *>(context)->SetY((float)value->value_fplacement.position.y);
	static_cast<Placement *>(context)->SetZ((float)value->value_fplacement.position.z);
	static_cast<Placement *>(context)->SetHeading((float)value->value_fplacement.orientation.heading);
	static_cast<Placement *>(context)->SetPitch((float)value->value_fplacement.orientation.pitch);
	static_cast<Placement *>(context)->SetRoll((float)value->value_fplacement.orientation.roll);
}
#pragma endregion

#pragma region SCS_VALUE_TYPE_fvector to Placement
SCSAPI_VOID telemetry_store_fvector(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context)
{
	if(!value) return;
	assert(value);
	assert(value->type == SCS_VALUE_TYPE_fvector);
	assert(context);
	//Placement * temp = static_cast<Placement *>(context);
	static_cast<Placement *>(context)->SetX((float)value->value_fvector.x);
	static_cast<Placement *>(context)->SetY((float)value->value_fvector.y);
	static_cast<Placement *>(context)->SetZ((float)value->value_fvector.z);
	static_cast<Placement *>(context)->SetHeading(0.00f);
	static_cast<Placement *>(context)->SetPitch(0.00f);
	static_cast<Placement *>(context)->SetRoll(0.00f);
}
#pragma endregion


#pragma region SCS_VALUE_TYPE_dplacement To Truck Position
SCSAPI_VOID telemetry_store_worldPlacement(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context)
{
	if (!value) return;
	assert(value);
	assert(value->type == SCS_VALUE_TYPE_dplacement);
	//assert(context);
	Telemetry::telemetry()->truck->worldPlacement.SetX((float)value->value_dplacement.position.x);
	Telemetry::telemetry()->truck->worldPlacement.SetY((float)value->value_dplacement.position.y);
	Telemetry::telemetry()->truck->worldPlacement.SetZ((float)value->value_dplacement.position.z);
	Telemetry::telemetry()->truck->worldPlacement.SetHeading((float)value->value_dplacement.orientation.heading);
	Telemetry::telemetry()->truck->worldPlacement.SetPitch((float)value->value_dplacement.orientation.pitch);
	Telemetry::telemetry()->truck->worldPlacement.SetRoll((float)value->value_dplacement.orientation.roll);
}
#pragma endregion

#pragma region SCS_VALUE_TYPE_dplacement To Trailer Position
SCSAPI_VOID telemetry_store_trailerWorldPlacement(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context)
{
	if (!value) return;
	assert(value);
	assert(value->type == SCS_VALUE_TYPE_dplacement);
	//assert(context);
	Telemetry::telemetry()->truck->trailerWorldPlacement.SetX((float)value->value_dplacement.position.x);
	Telemetry::telemetry()->truck->trailerWorldPlacement.SetY((float)value->value_dplacement.position.y);
	Telemetry::telemetry()->truck->trailerWorldPlacement.SetZ((float)value->value_dplacement.position.z);
	Telemetry::telemetry()->truck->trailerWorldPlacement.SetHeading((float)value->value_dplacement.orientation.heading);
	Telemetry::telemetry()->truck->trailerWorldPlacement.SetPitch((float)value->value_dplacement.orientation.pitch);
	Telemetry::telemetry()->truck->trailerWorldPlacement.SetRoll((float)value->value_dplacement.orientation.roll);
}
#pragma endregion

#pragma region SCS_VALUE_TYPE_string To std::string
SCSAPI_VOID telemetry_store_string(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context)
{
	if (!context) return;
	if (value)
	{
		*static_cast<std::string*>(context) = value->value_string.value;
	}
	else
		*static_cast<std::string*>(context) = "";
}
#pragma endregion

#pragma endregion

#pragma region Initialization Event
SCSAPI_RESULT scs_telemetry_init(const scs_u32_t version, const scs_telemetry_init_params_t *const params)
{
	readyToStart = false;


	if (version != SCS_TELEMETRY_VERSION_1_00)
		return SCS_RESULT_unsupported;

	const scs_telemetry_init_params_v100_t *const version_params = static_cast<const scs_telemetry_init_params_v100_t *>(params);

	if (version_params == NULL)
		return SCS_RESULT_generic_error;

	Debugger::init(version_params->common.log, "ETCARS.log");


#pragma region Set The ETCARS Plugin Version
	Telemetry::telemetry()->pluginVersion->majorVersion = 0;
	Telemetry::telemetry()->pluginVersion->minorVersion = 15;
	Telemetry::telemetry()->pluginVersion->pluginVersionOnlyStr = "0.15";
#pragma endregion

	//output version to the logs
	Debugger::log_line(std::string("[ Telemetry ]   ETCARS Version: " + std::to_string(Telemetry::telemetry()->pluginVersion->majorVersion) + "." + std::to_string(Telemetry::telemetry()->pluginVersion->minorVersion)).c_str());

#pragma region Get the OS Environment
	#ifdef _WIN32
		Telemetry::telemetry()->game->osEnvironment = "Windows";
	#endif
	#ifdef __linux__
		Telemetry::telemetry()->game->osEnvironment = "Linux";
	#endif
	#ifdef __APPLE__
		Telemetry::telemetry()->game->osEnvironment = "Mac OS X";
	#endif

	#if defined(SCS_ARCHITECTURE_x64)
		Telemetry::telemetry()->game->architecture = "x64";
	#elif defined (SCS_ARCHITECTURE_x86)
		Telemetry::telemetry()->game->architecture = "x86";
	#else
		#error "Must define either ENV32BIT or ENV64BIT".
	#endif
#pragma endregion


#pragma region Determine Game and Game Version
	if (strcmp(version_params->common.game_id, SCS_GAME_ID_EUT2) == 0) {

		const scs_u32_t MINIMAL_VERSION = SCS_TELEMETRY_EUT2_GAME_VERSION_1_00;
		if (version_params->common.game_version < MINIMAL_VERSION) {
			version_params->common.log(SCS_LOG_TYPE_error, "WARNING: Too old version of the game, some features might behave incorrectly");
		}

		const scs_u32_t IMPLEMENTED_VERSION = SCS_TELEMETRY_EUT2_GAME_VERSION_CURRENT;
		if (SCS_GET_MAJOR_VERSION(version_params->common.game_version) > SCS_GET_MAJOR_VERSION(IMPLEMENTED_VERSION)) {
			version_params->common.log(SCS_LOG_TYPE_error, "WARNING: Too new major version of the game, some features might behave incorrectly");
		}
		Telemetry::telemetry()->game->gameID = "ets2";
		Telemetry::telemetry()->game->gameName = "Euro Truck Simulator 2";

		Telemetry::telemetry()->game->majorVersion = SCS_GET_MAJOR_VERSION(version_params->common.game_version);
		Telemetry::telemetry()->game->minorVersion = SCS_GET_MINOR_VERSION(version_params->common.game_version);
		Telemetry::telemetry()->game->gameVersionStr = Telemetry::telemetry()->game->gameName + " v" + std::to_string(Telemetry::telemetry()->game->majorVersion) + '.' + std::to_string(Telemetry::telemetry()->game->minorVersion);
		Telemetry::telemetry()->game->gameVersionOnlyStr = std::to_string(Telemetry::telemetry()->game->majorVersion) + '.' + std::to_string(Telemetry::telemetry()->game->minorVersion);

	}
	else if (strcmp(version_params->common.game_id, SCS_GAME_ID_ATS) == 0) {

		const scs_u32_t MINIMAL_VERSION = SCS_TELEMETRY_ATS_GAME_VERSION_1_00;
		if (version_params->common.game_version < MINIMAL_VERSION) {
			version_params->common.log(SCS_LOG_TYPE_error, "WARNING: Too old version of the game, some features might behave incorrectly");
		}

		const scs_u32_t IMPLEMENTED_VERSION = SCS_TELEMETRY_ATS_GAME_VERSION_CURRENT;
		if (SCS_GET_MAJOR_VERSION(version_params->common.game_version) > SCS_GET_MAJOR_VERSION(IMPLEMENTED_VERSION)) {
			version_params->common.log(SCS_LOG_TYPE_error, "WARNING: Too new major version of the game, some features might behave incorrectly");
		}
		Telemetry::telemetry()->game->gameID = "ats";
		Telemetry::telemetry()->game->gameName = "American Truck Simulator";
		Telemetry::telemetry()->game->majorVersion = SCS_GET_MAJOR_VERSION(version_params->common.game_version);
		Telemetry::telemetry()->game->minorVersion = SCS_GET_MINOR_VERSION(version_params->common.game_version);
		Telemetry::telemetry()->game->gameVersionStr = Telemetry::telemetry()->game->gameName + " v" + std::to_string(Telemetry::telemetry()->game->majorVersion) + '.' + std::to_string(Telemetry::telemetry()->game->minorVersion);
		Telemetry::telemetry()->game->gameVersionOnlyStr = std::to_string(Telemetry::telemetry()->game->majorVersion) + '.' + std::to_string(Telemetry::telemetry()->game->minorVersion);
	}
	else {
		version_params->common.log(SCS_LOG_TYPE_error, "WARNING: Unsupported game, some features or values might behave incorrectly");
	}
#pragma endregion

	//output to the log
	Debugger::log_line(std::string("[ Telemetry ]   Game: " + Telemetry::telemetry()->game->gameVersionStr).c_str());

	const bool events_registered =
		(version_params->register_for_event(SCS_TELEMETRY_EVENT_frame_start, telemetry_frame_start, NULL) == SCS_RESULT_ok) &&
		(version_params->register_for_event(SCS_TELEMETRY_EVENT_frame_end, telemetry_frame_end, NULL) == SCS_RESULT_ok) &&
		(version_params->register_for_event(SCS_TELEMETRY_EVENT_paused, telemetry_pause, NULL) == SCS_RESULT_ok) &&
		(version_params->register_for_event(SCS_TELEMETRY_EVENT_started, telemetry_pause, NULL) == SCS_RESULT_ok)
		;

	if (!events_registered) {
		version_params->common.log(SCS_LOG_TYPE_error, "Unable to register event callbacks");
		return SCS_RESULT_generic_error;
	}

	version_params->register_for_event(SCS_TELEMETRY_EVENT_configuration, telemetry_configuration, NULL);

	//Register Channels
	registerChannel(CHANNEL_game_time, u32, Telemetry::telemetry()->game->dateTime);
	registerChannel(CHANNEL_local_scale, float, Telemetry::telemetry()->game->localScale);
	version_params->register_for_channel(SCS_TELEMETRY_TRUCK_CHANNEL_world_placement, SCS_U32_NIL, SCS_VALUE_TYPE_dplacement, SCS_TELEMETRY_CHANNEL_FLAG_no_value, telemetry_store_worldPlacement, NULL);
	version_params->register_for_channel(SCS_TELEMETRY_TRAILER_CHANNEL_world_placement, SCS_U32_NIL, SCS_VALUE_TYPE_dplacement, SCS_TELEMETRY_CHANNEL_FLAG_no_value, telemetry_store_trailerWorldPlacement, NULL);
	registerChannel(TRUCK_CHANNEL_cruise_control, float, Telemetry::telemetry()->truck->cruiseControlSpeed);
	registerChannel(TRUCK_CHANNEL_parking_brake, bool, Telemetry::telemetry()->truck->parkingBrake);
	registerChannel(TRUCK_CHANNEL_motor_brake, bool, Telemetry::telemetry()->truck->motorBrake);
	registerChannel(TRUCK_CHANNEL_brake_air_pressure_warning, bool, Telemetry::telemetry()->truck->warnings->airPressure);
	registerChannel(TRUCK_CHANNEL_brake_air_pressure_emergency, bool, Telemetry::telemetry()->truck->warnings->airPressureEmergency);
	registerChannel(TRUCK_CHANNEL_electric_enabled, bool, Telemetry::telemetry()->truck->electricsEnabled);
	registerChannel(TRUCK_CHANNEL_engine_enabled, bool, Telemetry::telemetry()->truck->engineEnabled);
	registerChannel(TRUCK_CHANNEL_odometer, float, Telemetry::telemetry()->truck->odometer);
	registerChannel(TRUCK_CHANNEL_speed, float, Telemetry::telemetry()->truck->speed);
	registerChannel(TRUCK_CHANNEL_fuel, float, Telemetry::telemetry()->truck->fuel->currentLitres);
	registerChannel(TRUCK_CHANNEL_fuel_warning, bool, Telemetry::telemetry()->truck->warnings->fuelLow);
	registerChannel(TRUCK_CHANNEL_oil_pressure_warning, bool, Telemetry::telemetry()->truck->warnings->oilPressure);
	registerChannel(TRUCK_CHANNEL_water_temperature_warning, bool, Telemetry::telemetry()->truck->warnings->waterTemperature);
	registerChannel(TRUCK_CHANNEL_battery_voltage_warning, bool, Telemetry::telemetry()->truck->warnings->batteryVoltage);
	registerChannel(TRUCK_CHANNEL_wear_engine, float, Telemetry::telemetry()->truck->damages->engine);
	registerChannel(TRUCK_CHANNEL_wear_transmission, float, Telemetry::telemetry()->truck->damages->transmission);
	registerChannel(TRUCK_CHANNEL_wear_cabin, float, Telemetry::telemetry()->truck->damages->cabin);
	registerChannel(TRUCK_CHANNEL_wear_chassis, float, Telemetry::telemetry()->truck->damages->chassis);
	registerChannel(TRUCK_CHANNEL_wear_wheels, float, Telemetry::telemetry()->truck->damages->wheels);
	registerChannel(TRUCK_CHANNEL_navigation_distance, float, Telemetry::telemetry()->navigation->distance);
  	registerChannel(TRUCK_CHANNEL_navigation_time, float, Telemetry::telemetry()->navigation->time);
	registerChannel(TRUCK_CHANNEL_navigation_speed_limit, float, Telemetry::telemetry()->navigation->speedLimit);
	registerChannel(CHANNEL_next_rest_stop, s32, Telemetry::telemetry()->game->nextRestStop);
	registerChannel(TRUCK_CHANNEL_light_beacon, bool, Telemetry::telemetry()->truck->lights->beacon);
	registerChannel(TRUCK_CHANNEL_light_aux_front, u32, Telemetry::telemetry()->truck->lights->frontAux);
	registerChannel(TRUCK_CHANNEL_light_aux_roof, u32, Telemetry::telemetry()->truck->lights->roofAux);
	registerChannel(TRUCK_CHANNEL_light_high_beam, bool, Telemetry::telemetry()->truck->lights->highBeam);
	registerChannel(TRUCK_CHANNEL_light_low_beam, bool, Telemetry::telemetry()->truck->lights->lowBeam);
	registerChannel(TRUCK_CHANNEL_engine_rpm, float, Telemetry::telemetry()->truck->engineRPM);
	registerChannel(TRUCK_CHANNEL_engine_gear, s32, Telemetry::telemetry()->truck->gear);
	registerChannel(TRUCK_CHANNEL_displayed_gear, s32, Telemetry::telemetry()->truck->gearDisplayed);
	registerChannel(TRUCK_CHANNEL_retarder_level, u32, Telemetry::telemetry()->truck->retarderBrakeLevel);
	registerChannel(TRUCK_CHANNEL_brake_temperature, float, Telemetry::telemetry()->truck->brakeTemperature);
	registerChannel(TRUCK_CHANNEL_fuel_average_consumption, float, Telemetry::telemetry()->truck->fuel->consumptionAverage);
	registerChannel(TRUCK_CHANNEL_fuel_range, float, Telemetry::telemetry()->truck->fuelRange);
	registerChannel(TRUCK_CHANNEL_oil_pressure, float, Telemetry::telemetry()->truck->oilPressure);
	registerChannel(TRUCK_CHANNEL_oil_temperature, float, Telemetry::telemetry()->truck->oilTemperature);
	registerChannel(TRUCK_CHANNEL_water_temperature, float, Telemetry::telemetry()->truck->waterTemperature);
	registerChannel(TRUCK_CHANNEL_battery_voltage, float, Telemetry::telemetry()->truck->batteryVoltage);
	registerChannel(TRUCK_CHANNEL_lblinker, bool, Telemetry::telemetry()->truck->lights->leftBlinker->isEnabled);
	registerChannel(TRUCK_CHANNEL_rblinker, bool, Telemetry::telemetry()->truck->lights->rightBlinker->isEnabled);
	registerChannel(TRUCK_CHANNEL_light_lblinker, bool, Telemetry::telemetry()->truck->lights->leftBlinker->isOn);
	registerChannel(TRUCK_CHANNEL_light_rblinker, bool, Telemetry::telemetry()->truck->lights->rightBlinker->isOn);
	registerChannel(TRUCK_CHANNEL_light_parking, bool, Telemetry::telemetry()->truck->lights->parking);
	registerChannel(TRUCK_CHANNEL_light_brake, bool, Telemetry::telemetry()->truck->lights->brake);
	registerChannel(TRUCK_CHANNEL_light_reverse, bool, Telemetry::telemetry()->truck->lights->reverse);
	registerChannel(TRUCK_CHANNEL_wipers, bool, Telemetry::telemetry()->truck->wipersOn);
	registerChannel(TRAILER_CHANNEL_connected, bool, Telemetry::telemetry()->truck->trailerConnected);
	registerChannel(TRUCK_CHANNEL_cabin_offset, fplacement, Telemetry::telemetry()->truck->cabinOffset);
	registerChannel(TRAILER_CHANNEL_wear_chassis, float, Telemetry::telemetry()->job->damage);
	registerChannel(TRUCK_CHANNEL_input_steering, float, Telemetry::telemetry()->truck->inputSteering);
	registerChannel(TRUCK_CHANNEL_input_throttle, float, Telemetry::telemetry()->truck->inputThrottle);
	registerChannel(TRUCK_CHANNEL_input_brake, float, Telemetry::telemetry()->truck->inputBrake);
	registerChannel(TRUCK_CHANNEL_input_clutch, float, Telemetry::telemetry()->truck->inputClutch);
	registerChannel(TRUCK_CHANNEL_effective_steering, float, Telemetry::telemetry()->truck->effectiveSteering);
	registerChannel(TRUCK_CHANNEL_effective_throttle, float, Telemetry::telemetry()->truck->effectiveThrottle);
	registerChannel(TRUCK_CHANNEL_effective_brake, float, Telemetry::telemetry()->truck->effectiveBrake);
	registerChannel(TRUCK_CHANNEL_effective_clutch, float, Telemetry::telemetry()->truck->effectiveClutch);
	registerChannel(TRUCK_CHANNEL_hshifter_slot, u32, Telemetry::telemetry()->truck->hShifterSlot);
	registerChannel(TRUCK_CHANNEL_brake_air_pressure, float, Telemetry::telemetry()->truck->brakeAirPressure);
	registerChannel(TRUCK_CHANNEL_dashboard_backlight, float, Telemetry::telemetry()->truck->dashboardBacklight);
	if(strcmp(version_params->common.game_id, SCS_GAME_ID_EUT2) == 0)
	{
		registerChannel(TRUCK_CHANNEL_adblue,float,Telemetry::telemetry()->truck->adBlue);
		//registerChannel(TRUCK_CHANNEL_adblue_average_consumption,float,Telemetry::telemetry()->truck->adBlueConsumptionAverage);
		registerChannel(TRUCK_CHANNEL_adblue_warning, bool, Telemetry::telemetry()->truck->warnings->adBlue);
	}

	version_params->register_for_channel(SCS_TELEMETRY_TRUCK_CHANNEL_cabin_angular_velocity, SCS_U32_NIL, SCS_VALUE_TYPE_fvector, SCS_TELEMETRY_CHANNEL_FLAG_no_value, telemetry_store_fvector, &Telemetry::telemetry()->truck->cabinAngularVelocity);
	version_params->register_for_channel(SCS_TELEMETRY_TRUCK_CHANNEL_cabin_angular_acceleration, SCS_U32_NIL, SCS_VALUE_TYPE_fvector, SCS_TELEMETRY_CHANNEL_FLAG_no_value, telemetry_store_fvector, &Telemetry::telemetry()->truck->cabinAngularVelocity);
	version_params->register_for_channel(SCS_TELEMETRY_TRUCK_CHANNEL_local_linear_velocity, SCS_U32_NIL, SCS_VALUE_TYPE_fvector, SCS_TELEMETRY_CHANNEL_FLAG_no_value, telemetry_store_fvector, &Telemetry::telemetry()->truck->linearVelocity);
	version_params->register_for_channel(SCS_TELEMETRY_TRUCK_CHANNEL_local_angular_velocity, SCS_U32_NIL, SCS_VALUE_TYPE_fvector, SCS_TELEMETRY_CHANNEL_FLAG_no_value, telemetry_store_fvector, &Telemetry::telemetry()->truck->angularVelocity);
	version_params->register_for_channel(SCS_TELEMETRY_TRUCK_CHANNEL_local_linear_acceleration, SCS_U32_NIL, SCS_VALUE_TYPE_fvector, SCS_TELEMETRY_CHANNEL_FLAG_no_value, telemetry_store_fvector, &Telemetry::telemetry()->truck->linearAcceleration);
	version_params->register_for_channel(SCS_TELEMETRY_TRUCK_CHANNEL_local_angular_acceleration, SCS_U32_NIL, SCS_VALUE_TYPE_fvector, SCS_TELEMETRY_CHANNEL_FLAG_no_value, telemetry_store_fvector, &Telemetry::telemetry()->truck->angularAcceleration);

	version_params->register_for_channel(SCS_TELEMETRY_TRAILER_CHANNEL_local_linear_velocity, SCS_U32_NIL, SCS_VALUE_TYPE_fvector, SCS_TELEMETRY_CHANNEL_FLAG_no_value, telemetry_store_fvector, &Telemetry::telemetry()->trailer->linearVelocity);
	version_params->register_for_channel(SCS_TELEMETRY_TRAILER_CHANNEL_local_angular_velocity, SCS_U32_NIL, SCS_VALUE_TYPE_fvector, SCS_TELEMETRY_CHANNEL_FLAG_no_value, telemetry_store_fvector, &Telemetry::telemetry()->trailer->angularVelocity);
	version_params->register_for_channel(SCS_TELEMETRY_TRAILER_CHANNEL_local_linear_acceleration, SCS_U32_NIL, SCS_VALUE_TYPE_fvector, SCS_TELEMETRY_CHANNEL_FLAG_no_value, telemetry_store_fvector, &Telemetry::telemetry()->trailer->linearAcceleration);
	version_params->register_for_channel(SCS_TELEMETRY_TRAILER_CHANNEL_local_angular_acceleration, SCS_U32_NIL, SCS_VALUE_TYPE_fvector, SCS_TELEMETRY_CHANNEL_FLAG_no_value, telemetry_store_fvector, &Telemetry::telemetry()->trailer->angularAcceleration);	
	Telemetry::telemetry()->lastTimestamp = static_cast<scs_timestamp_t>(-1);
	Telemetry::telemetry()->game->paused = true;
	Telemetry::telemetry()->game->isDriving = false;

	Telemetry::telemetry()->game->isMultiplayer = isMP();

	Telemetry::telemetry()->game->gameTimeTM = { 0 };
	Telemetry::telemetry()->game->gameTimeTM.tm_hour = 0;   Telemetry::telemetry()->game->gameTimeTM.tm_min = 0; Telemetry::telemetry()->game->gameTimeTM.tm_sec = 0;
	Telemetry::telemetry()->game->gameTimeTM.tm_year = 100; Telemetry::telemetry()->game->gameTimeTM.tm_mon = 0; Telemetry::telemetry()->game->gameTimeTM.tm_mday = 1;
	shuttingDown = false;
	endOfJSON = "\r";
	Job::Current()->game = Telemetry::telemetry()->game->gameName.c_str();
	Job::Current()->gameID = Telemetry::telemetry()->game->gameID.c_str();
	Job::Current()->gameVersion = Telemetry::telemetry()->game->gameVersionOnlyStr.c_str();
	Job::Current()->pluginVersion = Telemetry::telemetry()->pluginVersion->pluginVersionOnlyStr.c_str();
	Job::Current()->isMultiplayer = Telemetry::telemetry()->game->isMultiplayer;

	//Add the transport locations to the list...
	transportLocations.push_back(TransportLocation(0.00f,0.00f,0.00f,"SOMEWHERE_START",0.00f,0.00f,0.00f,"SOMEWHERE_END",0.00f,0.00f,"somestart","someend"));


	try
	{

		if (!steamAPIResult)
		{
			Debugger::log_line("FAILED TO INITIALIZE STEAM API SDK!!!!!");
		}
		else
		{
			SteamContext = CSteamAPIContext();
			if (!SteamContext.Init())
			{
				Debugger::log_line("FAILED TO INITIALIZE STEAM API CONTEXT!!!!!");
			}
			else
			{
				// Get the current users Steam info.
				steamUsername1 = SteamContext.SteamFriends()->GetPersonaName();//error: __imp_SteamFriends(...) returned nullptr
				CSteamID sID = SteamContext.SteamUser()->GetSteamID();//error: __imp_SteamUser(...) returned nullptr
				uint64 ssID = sID.ConvertToUint64();
				steamID1= std::to_string(ssID).c_str();
				//strcpy(Job::Current()->steamUsername, steamUsername);
				//strcpy(Job::Current()->steamID, sssID);
				Job::Current()->steamUsername = steamUsername1.c_str();
				Job::Current()->steamID = steamID1.c_str();
				

				bool endsInCommaDLC = false;
				int pos = 0;
				std::stringstream ss;
				ss << " [ Steam API ] SteamID: ";
				ss << Job::Current()->steamID;
				std::string steamidInfo = ss.str();
				Debugger::log_line(steamidInfo.c_str());
				ss.clear();
				ss.str("");
				std::stringstream ss2;
				ss2 << " [Steam API ] Steam Username: ";
				ss2 << Job::Current()->steamUsername;
				std::string steamusernameInfo = ss2.str();
				Debugger::log_line(steamusernameInfo.c_str());
				ss2.clear();
				ss2.str("");
				std::stringstream ss3;
				ss3 << "{ \"DLC\": [";

				int32 DLCCount = SteamContext.SteamApps()->GetDLCCount();
				for (int i = 0; i < DLCCount; ++i)
				{
					AppId_t appId;
					bool available;
					char name[128];
					bool success = SteamContext.SteamApps()->BGetDLCDataByIndex(i, &appId, &available, name, 128);
					if (success) {
						bool installed = SteamContext.SteamApps()->BIsDlcInstalled(appId);

						ss3 << "{";
						ss3 << "\"appid\": \"" << appId << "\",";
						ss3 << "\"name\": \"" << name << "\",";
						ss3 << "\"available\": " << boolToString(available) << ",";
						ss3 << "\"installed\": " << boolToString(installed) << "";
						ss3 << "}";
					
						if (i < DLCCount - 1)
						{
							ss3 << ",";
							endsInCommaDLC = true;
							pos = (int)strlen(ss3.str().c_str());
						}
						else
						{
							ss3 << "";
							endsInCommaDLC = false;
						}
					}

				}
				ss3 << "]}";
				std::string tmpDLC = ss3.str();
				if(endsInCommaDLC)
					tmpDLC.replace(pos-1, 1, "");
				steamDLCJSON1 = tmpDLC.c_str();
				Job::Current()->steamDLCJSON = steamDLCJSON1.c_str();
				ss3.str("");
				std::stringstream ss4;
				ss4 << " [ Steam API ] " << Job::Current()->steamDLCJSON;
				//Debugger::log_line(ss4.str().c_str());
				ss4.str("");
			}
		}
	}
	catch (std::exception ex)
	{


	}
	register_for_channel = version_params->register_for_channel;
	unregister_from_channel = version_params->unregister_from_channel;

	
	io_service.reset();
	s = new tcp_server(io_service);
	bt = new std::thread([&] { io_service.run(); });

	SendStatusMessage("INITIALIZED",true);
	telemBCastThread = boost::thread(TelemetryWorking);
	curl_global_init(CURL_GLOBAL_ALL);
	api = new API();
	return SCS_RESULT_ok;


}
#pragma endregion


#pragma region Closing and Shutdown functions, the both of them should be the same

#pragma region Shutdown Event
SCSAPI_VOID scs_telemetry_shutdown(void)
{
	Debugger::log_line("TELEMETRY SHUT DOWN");
	shuttingDown = true;
	telemBCastThread.join();
	SendStatusMessage("TELEMETRY SHUT DOWN", true);
	
	s->interrupt();
	while (s->IsShutDownCompleted() == false)
	{
#ifdef _WIN32
		Sleep(1);
#endif
#if defined(__linux__) || defined(__APPLE__)
		sleep(1);
#endif

	}
	Debugger::log_line("Broadcast Server Thread Shut Down and Joined");
	Debugger::log_line("Waiting for IO Service to Stop...");;
	io_service.stop();
	bt->join();
	Debugger::log_line("IO Service Stopped");


	delete s;
	delete bt;
	delete api;


	//quick save to log....
	if (Job::Current()->onJob)
	{
		shutdownJob.topSpeed = Job::Current()->topSpeed;
		shutdownJob.wasSpeeding = Job::Current()->wasSpeeding;
		shutdownJob.speedingCount = Job::Current()->speedingCount;
		shutdownJob.distanceDriven = Job::Current()->distanceDriven;
		shutdownJob.fuelBurned = Job::Current()->fuelBurned;
		shutdownJob.fuelPurchased = Job::Current()->fuelPurchased;
		shutdownJob.collisionCount = Job::Current()->collisionCount;
		shutdownJob.totalTrailerDamage = Job::Current()->totalTrailerDamage;
		shutdownJob.totalEngineDamage = Job::Current()->totalEngineDamage;
		shutdownJob.totalTransmissionDamage = Job::Current()->totalTransmissionDamage;
		shutdownJob.totalCabinDamage = Job::Current()->totalCabinDamage;
		shutdownJob.totalChassisDamage = Job::Current()->totalChassisDamage;
		shutdownJob.totalWheelDamage = Job::Current()->totalWheelDamage;
		shutdownJob.realTimeTaken = Job::Current()->realTimeTaken;
		shutdownJob.transportLocs = Telemetry::telemetry()->job->transportLocations;
		shutdownJob.lastFuel = Job::Current()->fuel;
		shutdownJob.lastOdometer = Job::Current()->odometer;
		shutdownJob.cabinDamage = Job::Current()->cabinDamage;
		shutdownJob.chassisDamage = Job::Current()->chassisDamage;
		shutdownJob.engineDamage = Job::Current()->engineDamage;
		shutdownJob.transmissionDamage = Job::Current()->transmissionDamage;
		shutdownJob.wheelDamage = Job::Current()->wheelDamage;
		shutdownJob.trailerDamage = Job::Current()->trailerDamage;
	}
	doSaveETCARSFile();
	Debugger::log_line("SAVED ETCARS FILE");
	Debugger::log_line(std::to_string(shutdownJob.distanceDriven).c_str());
	//---------------------

	Debugger::finish_log();
	//io_service.reset();
	SteamContext.Clear();
	curl_global_cleanup();

}
#pragma endregion

#pragma region Cleanup/Unload
#ifdef _WIN32
BOOL APIENTRY DllMain(
	HMODULE module,
	DWORD  reason_for_call,
	LPVOID reseved
)
{
	
	if (reason_for_call == DLL_PROCESS_DETACH)
	{
		if (!shuttingDown)
		{
			Debugger::log_line("TELEMETRY SHUT DOWN - DETATCH");
			shuttingDown = true;
			telemBCastThread.join();
			SendStatusMessage("TELEMETRY SHUT DOWN", true);

			s->interrupt();
			while (s->IsShutDownCompleted() == false)
			{
				Sleep(1);
			}
			Debugger::log_line("Broadcast Server Thread Shut Down and Joined - DETATCH");
			Debugger::log_line("Waiting for IO Service to Stop... - DETATCH");
			
			io_service.stop();
			bt->join();
			Debugger::log_line("IO Service Stopped - DETATCH");
			delete s;
			delete bt;
			//FreeLibrary(saveLoaded);
			Debugger::finish_log();
			SteamAPI_Shutdown();
			curl_global_cleanup();


		}

	}
	else if (reason_for_call == DLL_PROCESS_ATTACH)
	{
		hmod = module;
		curl_global_init(CURL_GLOBAL_ALL);
		steamAPIResult = SteamAPI_InitSafe();
	}
	return TRUE;
}
#endif

#if  defined(__linux__) || defined(__APPLE__)
void __attribute__((destructor)) unload(void)
{
	if (!shuttingDown)
	{
		Debugger::log_line("TELEMETRY SHUT DOWN - DETATCH");
		shuttingDown = true;
		telemBCastThread.join();
		SendStatusMessage("TELEMETRY SHUT DOWN", true);

		s->interrupt();
		while (s->IsShutDownCompleted() == false)
		{
			sleep(1);
		}
		Debugger::log_line("Broadcast Server Thread Shut Down and Joined - DETATCH");
		Debugger::log_line("Waiting for IO Service to Stop... - DETATCH");

		io_service.stop();
		bt->join();
		Debugger::log_line("IO Service Stopped - DETATCH");
		delete s;
		delete bt;
		Debugger::finish_log();
		SteamAPI_Shutdown();
		curl_global_cleanup();
	}
}
void __attribute__((constructor)) load(void)
{
	//hmod = module;
	curl_global_init(CURL_GLOBAL_ALL);
	steamAPIResult = SteamAPI_InitSafe();
}
#endif

#pragma endregion

#pragma endregion

#pragma region Damage Change Check
bool changeInDamages(float old[5], float next[5])
{
	for (int i = 0; i < 5; i++)
	{
		if (old[i] < next[i])
		{
			if (next[i] - old[i] > 0.00001)
				return true;
		}

	}
	return false;
}
#pragma endregion

bool isMP()
{
#ifdef _WIN32
	//char atsbuf[1024];
	//char ets2buf[1024];
	bool val = false;
	HMODULE ats_hmodule = GetModuleHandleA("core_atsmp.dll");
	HMODULE ets2_hmodule = GetModuleHandleA("core_ets2mp.dll");
	if (ats_hmodule == NULL && ets2_hmodule == NULL)
		val = false;
	else
		val = true;
	return val;
#endif

#if defined(__linux__) || defined(__APPLE__)
	return false;
#endif

}

std::string boolToString(bool bol)
{
	if (bol == true)
	{
		return "true";
	}
	else
	{
		return "false";
	}
}

std::string generateTelemetryJSON(const char* status)
{
	std::string telemJSON;
	std::stringstream telemBCastSS;


	telemBCastSS << "{";
	telemBCastSS << "\"data\": {";
	telemBCastSS << "\"status\": \"" << status << "\",";
	telemBCastSS << "\"telemetry\": {";
	telemBCastSS << "\"pluginVersion\": {";
	telemBCastSS << "\"majorVersion\":" << Telemetry::telemetry()->pluginVersion->majorVersion << ",";
	telemBCastSS << "\"minorVersion\":" << Telemetry::telemetry()->pluginVersion->minorVersion << ",";
	telemBCastSS << "\"pluginVersionOnlyStr\": \"" << Telemetry::telemetry()->pluginVersion->pluginVersionOnlyStr << "\"";
	telemBCastSS << "}," << "\"game\": {";
	telemBCastSS << "\"isMultiplayer\": " << boolToString(Telemetry::telemetry()->game->isMultiplayer) << ",";
	telemBCastSS << "\"paused\": " << boolToString(Telemetry::telemetry()->game->paused) << ",";
	telemBCastSS << "\"isDriving\": " << boolToString(Telemetry::telemetry()->game->isDriving) << ",";
	telemBCastSS << "\"majorVersion\": " << Telemetry::telemetry()->game->majorVersion << ",";
	telemBCastSS << "\"minorVersion\": " << Telemetry::telemetry()->game->minorVersion << ",";
	telemBCastSS << "\"gameID\": \"" << Telemetry::telemetry()->game->gameID << "\",";
	telemBCastSS << "\"gameName\": \"" << Telemetry::telemetry()->game->gameName << "\",";
	telemBCastSS << "\"gameVersionStr\": \"" << Telemetry::telemetry()->game->gameVersionStr << "\",";
	telemBCastSS << "\"gameVersionOnly\": \"" << Telemetry::telemetry()->game->gameVersionOnlyStr << "\",";
	telemBCastSS << "\"nextRestStop\": " << Telemetry::telemetry()->game->nextRestStop << ",";
	telemBCastSS << "\"gameDateTime\": " << Telemetry::telemetry()->game->dateTime << ",";
	telemBCastSS << "\"gameDayTime\": \"" << convertToDayTime(Telemetry::telemetry()->game->dateTime) << "\",";
	telemBCastSS << "\"gameDateTimeStr\": \"" << convertToDateTime(Telemetry::telemetry()->game->dateTime) << "\",";
	telemBCastSS << "\"osEnvironment\": \"" << Telemetry::telemetry()->game->osEnvironment << "\",";
	telemBCastSS << "\"architecture\": \"" << Telemetry::telemetry()->game->architecture << "\",";

	telemBCastSS << "\"localScale\": " << Telemetry::telemetry()->game->localScale << ",";
	telemBCastSS << "\"substances\": [";
	if(Telemetry::telemetry()->game->substanceLock.try_lock())
	{
		for(int i = 0; i < Telemetry::telemetry()->game->substances.size(); i++)
		{
			telemBCastSS << "\"" << Telemetry::telemetry()->game->substances.at(i) << "\"";
			if(i < Telemetry::telemetry()->game->substances.size()-1)
			{
				telemBCastSS << ",";
			}
		}
		Telemetry::telemetry()->game->substanceLock.unlock();
	}
	telemBCastSS << "]";
	telemBCastSS << "},";
	telemBCastSS << "\"truck\": {";
	telemBCastSS << "\"cruiseControlSpeed\": " << Telemetry::telemetry()->truck->cruiseControlSpeed << ",";
	telemBCastSS << "\"gear\": " << Telemetry::telemetry()->truck->gear << ",";
	telemBCastSS << "\"gearDisplayed\": " << Telemetry::telemetry()->truck->gearDisplayed << ",";
	telemBCastSS << "\"retarderBrakeLevel\": " << Telemetry::telemetry()->truck->retarderBrakeLevel << ",";
	telemBCastSS << "\"wipersOn\": " << boolToString(Telemetry::telemetry()->truck->wipersOn) << ",";
	telemBCastSS << "\"make\": \"" << Telemetry::telemetry()->truck->make << "\",";
	telemBCastSS << "\"makeID\": \"" << Telemetry::telemetry()->truck->makeID << "\",";
	telemBCastSS << "\"model\": \"" << Telemetry::telemetry()->truck->model << "\",";
	telemBCastSS << "\"modelID\": \"" << Telemetry::telemetry()->truck->modelID << "\",";
	telemBCastSS << "\"shifterType\": \"" << Telemetry::telemetry()->truck->shifterType << "\",";
	telemBCastSS << "\"odometer\": " << Telemetry::telemetry()->truck->odometer << ",";
	telemBCastSS << "\"hasTruck\": " << boolToString(Telemetry::telemetry()->truck->hasTruck) << ",";
	telemBCastSS << "\"engineEnabled\": " << boolToString(Telemetry::telemetry()->truck->engineEnabled) << ",";
	telemBCastSS << "\"electricsEnabled\": " << boolToString(Telemetry::telemetry()->truck->electricsEnabled) << ",";
	telemBCastSS << "\"motorBrake\": " << boolToString(Telemetry::telemetry()->truck->motorBrake) << ",";
	telemBCastSS << "\"parkingBrake\": " << boolToString(Telemetry::telemetry()->truck->parkingBrake) << ", ";
	telemBCastSS << "\"speed\": " << Telemetry::telemetry()->truck->speed << ",";
	telemBCastSS << "\"engineRPM\": " << Telemetry::telemetry()->truck->engineRPM << ",";
	telemBCastSS << "\"brakeTemperature\": " << Telemetry::telemetry()->truck->brakeTemperature << ",";
	telemBCastSS << "\"fuelRange\": " << Telemetry::telemetry()->truck->fuelRange << ",";
	telemBCastSS << "\"oilPressure\": " << Telemetry::telemetry()->truck->oilPressure << ",";
	telemBCastSS << "\"oilTemperature\": " << Telemetry::telemetry()->truck->oilTemperature << ",";
	telemBCastSS << "\"waterTemperature\": " << Telemetry::telemetry()->truck->waterTemperature << ",";
	telemBCastSS << "\"batteryVoltage\": " << Telemetry::telemetry()->truck->batteryVoltage << ",";

	telemBCastSS << "\"inputSteering\": " << Telemetry::telemetry()->truck->inputSteering << ",";
	telemBCastSS << "\"inputThrottle\": " << Telemetry::telemetry()->truck->inputThrottle << ",";
	telemBCastSS << "\"inputBrake\": " << Telemetry::telemetry()->truck->inputBrake << ",";
	telemBCastSS << "\"inputClutch\": " << Telemetry::telemetry()->truck->inputClutch << ",";
	telemBCastSS << "\"effectiveSteering\": " << Telemetry::telemetry()->truck->effectiveSteering << ",";
	telemBCastSS << "\"effectiveThrottle\": " << Telemetry::telemetry()->truck->effectiveThrottle << ",";
	telemBCastSS << "\"effectiveBrake\": " << Telemetry::telemetry()->truck->effectiveBrake << ",";
	telemBCastSS << "\"effectiveClutch\": " << Telemetry::telemetry()->truck->effectiveClutch << ",";
	telemBCastSS << "\"hShifterSlot\": " << Telemetry::telemetry()->truck->hShifterSlot << ",";
	telemBCastSS << "\"brakeAirPressure\": " << Telemetry::telemetry()->truck->brakeAirPressure << ",";
	telemBCastSS << "\"adBlue\": " << Telemetry::telemetry()->truck->adBlue << ",";
	//telemBCastSS << "\"adBlueConsumptionAverage\": " << Telemetry::telemetry()->truck->adBlueConsumptionAverage << ",";
	telemBCastSS << "\"dashboardBacklight\": " << Telemetry::telemetry()->truck->dashboardBacklight << ",";


	telemBCastSS << "\"maxEngineRPM\": " << Telemetry::telemetry()->truck->maxEngineRPM << ",";
	telemBCastSS << "\"forwardGearCount\": " << Telemetry::telemetry()->truck->forwardGearCount << ",";
	telemBCastSS << "\"reverseGearCount\": " << Telemetry::telemetry()->truck->reverseGearCount << ",";
	telemBCastSS << "\"retarderStepCount\": " << Telemetry::telemetry()->truck->retarderStepCount << ",";
	telemBCastSS << "\"trailerConnected\": " << boolToString(Telemetry::telemetry()->truck->trailerConnected) << ",";
	telemBCastSS << "\"worldPlacement\": { ";
	telemBCastSS << "\"x\": " << Telemetry::telemetry()->truck->worldPlacement.GetX() << ",";
	telemBCastSS << "\"y\": " << Telemetry::telemetry()->truck->worldPlacement.GetY() << ",";
	telemBCastSS << "\"z\": " << Telemetry::telemetry()->truck->worldPlacement.GetZ() << ",";
	telemBCastSS << "\"heading\": " << Telemetry::telemetry()->truck->worldPlacement.GetHeading() << ",";
	telemBCastSS << "\"pitch\": " << Telemetry::telemetry()->truck->worldPlacement.GetPitch() << ",";
	telemBCastSS << "\"roll\": " << Telemetry::telemetry()->truck->worldPlacement.GetRoll() << "";
	telemBCastSS << "},";
	telemBCastSS << "\"linearVelocity\": { ";
	telemBCastSS << "\"x\": " << Telemetry::telemetry()->truck->linearVelocity.GetX() << ",";
	telemBCastSS << "\"y\": " << Telemetry::telemetry()->truck->linearVelocity.GetY() << ",";
	telemBCastSS << "\"z\": " << Telemetry::telemetry()->truck->linearVelocity.GetZ() << "";
	telemBCastSS << "},";
	telemBCastSS << "\"angularVelocity\": { ";
	telemBCastSS << "\"x\": " << Telemetry::telemetry()->truck->angularVelocity.GetX() << ",";
	telemBCastSS << "\"y\": " << Telemetry::telemetry()->truck->angularVelocity.GetY() << ",";
	telemBCastSS << "\"z\": " << Telemetry::telemetry()->truck->angularVelocity.GetZ() << "";
	telemBCastSS << "},";
	telemBCastSS << "\"linearAcceleration\": { ";
	telemBCastSS << "\"x\": " << Telemetry::telemetry()->truck->linearAcceleration.GetX() << ",";
	telemBCastSS << "\"y\": " << Telemetry::telemetry()->truck->linearAcceleration.GetY() << ",";
	telemBCastSS << "\"z\": " << Telemetry::telemetry()->truck->linearAcceleration.GetZ() << "";
	telemBCastSS << "},";
	telemBCastSS << "\"angularAcceleration\": { ";
	telemBCastSS << "\"x\": " << Telemetry::telemetry()->truck->angularAcceleration.GetX() << ",";
	telemBCastSS << "\"y\": " << Telemetry::telemetry()->truck->angularAcceleration.GetY() << ",";
	telemBCastSS << "\"z\": " << Telemetry::telemetry()->truck->angularAcceleration.GetZ() << "";
	telemBCastSS << "},";
	telemBCastSS << "\"cabinOffset\": { ";
	telemBCastSS << "\"x\": " << Telemetry::telemetry()->truck->cabinOffset.GetX() << ",";
	telemBCastSS << "\"y\": " << Telemetry::telemetry()->truck->cabinOffset.GetY() << ",";
	telemBCastSS << "\"z\": " << Telemetry::telemetry()->truck->cabinOffset.GetZ() << ",";
	telemBCastSS << "\"heading\": " << Telemetry::telemetry()->truck->cabinOffset.GetHeading() << ",";
	telemBCastSS << "\"pitch\": " << Telemetry::telemetry()->truck->cabinOffset.GetPitch() << ",";
	telemBCastSS << "\"roll\": " << Telemetry::telemetry()->truck->cabinOffset.GetRoll() << "";
	telemBCastSS << "},";
	if(Telemetry::telemetry()->truck->hasTruck)
		telemBCastSS << "\"hookPosition\": { \"x\": " << Telemetry::telemetry()->truck->hookPosition.GetX() << ", \"y\": " << Telemetry::telemetry()->truck->hookPosition.GetY() << ", \"z\": " << Telemetry::telemetry()->truck->hookPosition.GetZ() << " },";
	else
		telemBCastSS << "\"hookPosition\": { \"x\": " << 0.00f << ", \"y\": " << 0.00f << ", \"z\": " << 0.00f << " },";
	if(Telemetry::telemetry()->truck->hasTruck)
		telemBCastSS << "\"headPosition\": { \"x\": " << Telemetry::telemetry()->truck->headPosition.GetX() << ", \"y\": " << Telemetry::telemetry()->truck->headPosition.GetY() << ", \"z\": " << Telemetry::telemetry()->truck->headPosition.GetZ() << " },";
	else
		telemBCastSS << "\"headPosition\": { \"x\": " << 0.00f << ", \"y\": " << 0.00f << ", \"z\": " << 0.00f << " },";

	telemBCastSS << "\"cabinAngularVelocity\": { \"x\": " << Telemetry::telemetry()->truck->cabinAngularVelocity.GetX() << ", \"y\": " << Telemetry::telemetry()->truck->cabinAngularVelocity.GetY() << ", \"z\": " << Telemetry::telemetry()->truck->cabinAngularVelocity.GetZ() << " },";
	telemBCastSS << "\"cabinAngularAcceleration\": { \"x\": " << Telemetry::telemetry()->truck->cabinAngularAcceleration.GetX() << ", \"y\": " << Telemetry::telemetry()->truck->cabinAngularAcceleration.GetY() << ", \"z\": " << Telemetry::telemetry()->truck->cabinAngularAcceleration.GetZ() << " },";
	
	telemBCastSS << "\"forwardRatios\": [ " ;
	if(Telemetry::telemetry()->truck->ratioMutex.try_lock())
	{
		for(int i = 0; i < Telemetry::telemetry()->truck->forwardRatios.size(); i++)
		{
			telemBCastSS << Telemetry::telemetry()->truck->forwardRatios.at(i);
			if(i < Telemetry::telemetry()->truck->forwardRatios.size()-1)
			{
				telemBCastSS << ", ";
			}
		}
		Telemetry::telemetry()->truck->ratioMutex.unlock();
	}
	telemBCastSS << " ],";

	telemBCastSS << "\"reverseRatios\": [ " ;
	if(Telemetry::telemetry()->truck->ratioMutex.try_lock())
	{
		for(int i = 0; i < Telemetry::telemetry()->truck->reverseRatios.size(); i++)
		{
			telemBCastSS << Telemetry::telemetry()->truck->reverseRatios.at(i);
			if(i < Telemetry::telemetry()->truck->reverseRatios.size()-1)
			{
				telemBCastSS << ", ";
			}
		}
		Telemetry::telemetry()->truck->ratioMutex.unlock();
	}
	telemBCastSS << " ],";


	//FOR EACH WHEEL
	if(Telemetry::telemetry()->truck->wheelCountLock.try_lock() && Telemetry::telemetry()->game->substanceLock.try_lock())
	{
		telemBCastSS << "\"wheelCount\": " << Telemetry::telemetry()->truck->wheelCount << ",";
		telemBCastSS << "\"wheelInfo\": [" << "";
		for(unsigned int a = 0; a < Telemetry::telemetry()->truck->wheelCount; a++)
		{


			telemBCastSS << "{";
			telemBCastSS << "\"suspensionDeflection\": " << Telemetry::telemetry()->truck->suspensionDeflections[a] << ",";
			telemBCastSS << "\"onGround\": " << boolToString(Telemetry::telemetry()->truck->wheelOnGround[a]) << ",";
			try {
				telemBCastSS << "\"substance\": \"" << Telemetry::telemetry()->game->substances.at(Telemetry::telemetry()->truck->wheelSubstance[a]) << "\",";
			}
			catch(std::exception ex)
			{
				telemBCastSS << "\"substance\": \"" << "static" << "\",";
			}
			telemBCastSS << "\"angularVelocity\": " << Telemetry::telemetry()->truck->wheelAngularVelocity[a] << ",";
			telemBCastSS << "\"lift\": " << Telemetry::telemetry()->truck->wheelLift[a] << ",";
			telemBCastSS << "\"liftOffset\": " << Telemetry::telemetry()->truck->wheelLiftOffset[a] << ",";
			telemBCastSS << "\"position\": {" << "\"x\": " << Telemetry::telemetry()->truck->wheelPosition[a].GetX() << ", \"y\": " << Telemetry::telemetry()->truck->wheelPosition[a].GetY() << ", \"z\": " << Telemetry::telemetry()->truck->wheelPosition[a].GetZ() << " },";
			telemBCastSS << "\"steerable\": " << boolToString(Telemetry::telemetry()->truck->wheelSteerable[a]) << ",";
			telemBCastSS << "\"simulated\": " << boolToString(Telemetry::telemetry()->truck->wheelSimulated[a]) << ",";
			telemBCastSS << "\"radius\": " << Telemetry::telemetry()->truck->wheelRadius[a] << ",";
			telemBCastSS << "\"steering\": " << Telemetry::telemetry()->truck->wheelSteering[a] << ",";
			telemBCastSS << "\"rotation\": " << Telemetry::telemetry()->truck->wheelRotation[a] << ",";
			telemBCastSS << "\"powered\": " << boolToString(Telemetry::telemetry()->truck->wheelPowered[a]) << ",";
			telemBCastSS << "\"liftable\": " << boolToString(Telemetry::telemetry()->truck->wheelLiftable[a]) << "";
			telemBCastSS << "}";

			if(a < Telemetry::telemetry()->truck->wheelCount-1)
			{
				telemBCastSS << ",";
			}
			else
			{
				telemBCastSS << "";
			}
		}
		telemBCastSS << "],";
		Telemetry::telemetry()->truck->wheelCountLock.unlock();
		Telemetry::telemetry()->game->substanceLock.unlock();
	}
	else
	{
		telemBCastSS << "\"wheelCount\": " << MAX_WHEEL_COUNT << ",";

		telemBCastSS << "\"wheelInfo\": [" << "";
		for(int a = 0; a < MAX_WHEEL_COUNT; a++)
		{
			telemBCastSS << "{";
			telemBCastSS << "\"suspensionDeflection\": " << 0.00f << ",";
			telemBCastSS << "\"onGround\": " << boolToString(false) << ",";
			telemBCastSS << "\"substance\": \"" << "static" << "\",";
			
			telemBCastSS << "\"angularVelocity\": " << 0.00f << ",";
			telemBCastSS << "\"lift\": " << 0.00f << ",";
			telemBCastSS << "\"liftOffset\": " << 0.00f << ",";
			telemBCastSS << "\"position\": {" << "\"x\": " << 0.00f << ", \"y\": " << 0.00f << ", \"z\": " << 0.00f << " },";
			telemBCastSS << "\"steerable\": " << boolToString(false) << ",";
			telemBCastSS << "\"simulated\": " << boolToString(false) << ",";
			telemBCastSS << "\"radius\": " << 0.00f << ",";
			telemBCastSS << "\"steering\": " << 0.00f << ",";
			telemBCastSS << "\"rotation\": " << 0.00f << ",";
			telemBCastSS << "\"powered\": " << boolToString(false) << ",";
			telemBCastSS << "\"liftable\": " << boolToString(false) << "";
			telemBCastSS << "}";

			if(a < MAX_WHEEL_COUNT-1)
			{
				telemBCastSS << ",";
			}
			else
			{
				telemBCastSS << "";
			}
		}
		telemBCastSS << "],";
	}

	



	telemBCastSS << "\"trailerPlacement\": {";
	telemBCastSS << "\"x\": " << Telemetry::telemetry()->truck->trailerWorldPlacement.GetX() << ",";
	telemBCastSS << "\"y\": " << Telemetry::telemetry()->truck->trailerWorldPlacement.GetY() << ",";
	telemBCastSS << "\"z\": " << Telemetry::telemetry()->truck->trailerWorldPlacement.GetZ() << ",";
	telemBCastSS << "\"heading\": " << Telemetry::telemetry()->truck->trailerWorldPlacement.GetHeading() << ",";
	telemBCastSS << "\"pitch\": " << Telemetry::telemetry()->truck->trailerWorldPlacement.GetPitch() << ",";
	telemBCastSS << "\"roll\": " << Telemetry::telemetry()->truck->trailerWorldPlacement.GetRoll() << "";
	telemBCastSS << "},";
	telemBCastSS << "\"warnings\": {";
	telemBCastSS << "\"batteryVoltage\": " << boolToString(Telemetry::telemetry()->truck->warnings->batteryVoltage) << ",";
	telemBCastSS << "\"airPressure\": " << boolToString(Telemetry::telemetry()->truck->warnings->airPressure) << ",";
	telemBCastSS << "\"airPressureEmergency\": " << boolToString(Telemetry::telemetry()->truck->warnings->airPressureEmergency) << ",";
	telemBCastSS << "\"oilPressure\": " << boolToString(Telemetry::telemetry()->truck->warnings->oilPressure) << ",";
	telemBCastSS << "\"waterTemperature\": " << boolToString(Telemetry::telemetry()->truck->warnings->waterTemperature) << ",";
	telemBCastSS << "\"fuelLow\": " << boolToString(Telemetry::telemetry()->truck->warnings->fuelLow) << ",";
	telemBCastSS << "\"adBlue\": " << boolToString(Telemetry::telemetry()->truck->warnings->adBlue) << "";
	telemBCastSS << "},";
	telemBCastSS << "\"damages\": {";
	telemBCastSS << "\"engine\": " << Telemetry::telemetry()->truck->damages->engine << ",";
	telemBCastSS << "\"transmission\": " << Telemetry::telemetry()->truck->damages->transmission << ",";
	telemBCastSS << "\"cabin\": " << Telemetry::telemetry()->truck->damages->cabin << ",";
	telemBCastSS << "\"chassis\": " << Telemetry::telemetry()->truck->damages->chassis << ",";
	telemBCastSS << "\"wheels\": " << Telemetry::telemetry()->truck->damages->wheels << ",";
	telemBCastSS << "\"trailer\": " << Telemetry::telemetry()->job->damage << "";
	telemBCastSS << "},";
	telemBCastSS << "\"lights\": {";
	telemBCastSS << "\"lowBeam\": " << boolToString(Telemetry::telemetry()->truck->lights->lowBeam) << ",";
	telemBCastSS << "\"highBeam\": " << boolToString(Telemetry::telemetry()->truck->lights->highBeam) << ",";
	telemBCastSS << "\"frontAux\": " << Telemetry::telemetry()->truck->lights->frontAux << ",";
	telemBCastSS << "\"beacon\": " << boolToString(Telemetry::telemetry()->truck->lights->beacon) << ",";
	telemBCastSS << "\"parking\": " << boolToString(Telemetry::telemetry()->truck->lights->parking) << ",";
	telemBCastSS << "\"brake\": " << boolToString(Telemetry::telemetry()->truck->lights->brake) << ",";
	telemBCastSS << "\"reverse\": " << boolToString(Telemetry::telemetry()->truck->lights->reverse) << ",";
	telemBCastSS << "\"leftBlinkerEnabled\": " << boolToString(Telemetry::telemetry()->truck->lights->leftBlinker->isEnabled) << ",";
	telemBCastSS << "\"rightBlinkerEnabled\": " << boolToString(Telemetry::telemetry()->truck->lights->rightBlinker->isEnabled) << ",";
	telemBCastSS << "\"leftBlinkerOn\": " << boolToString(Telemetry::telemetry()->truck->lights->leftBlinker->isOn) << ",";
	telemBCastSS << "\"rightBlinkerOn\": " << boolToString(Telemetry::telemetry()->truck->lights->rightBlinker->isOn) << ",";
	telemBCastSS << "\"roofAux\": " << Telemetry::telemetry()->truck->lights->roofAux << "";
	telemBCastSS << "},";
	telemBCastSS << "\"fuel\": {";
	telemBCastSS << "\"capacity\": " << Telemetry::telemetry()->truck->fuel->capacity << ",";
	telemBCastSS << "\"warningLevel\": " << Telemetry::telemetry()->truck->fuel->warningLevel << ",";
	telemBCastSS << "\"consumptionAverage\": " << Telemetry::telemetry()->truck->fuel->consumptionAverage << ",";
	telemBCastSS << "\"currentLitres\": " << Telemetry::telemetry()->truck->fuel->currentLitres << "";
	telemBCastSS << "}";
	telemBCastSS << "},";
	telemBCastSS << "\"navigation\": {";
	telemBCastSS << "\"distance\": " << Telemetry::telemetry()->navigation->distance << ",";
	telemBCastSS << "\"time\": " << Telemetry::telemetry()->navigation->time << ",";
	telemBCastSS << "\"lowestDistance\": " << Telemetry::telemetry()->navigation->lowestDistance << ",";
	telemBCastSS << "\"speedLimit\": " << Telemetry::telemetry()->navigation->speedLimit << "";
	telemBCastSS << "},";
	telemBCastSS << "\"job\": {";
	telemBCastSS << "\"cargoID\": \"" << Telemetry::telemetry()->job->cargoID << "\",";
	telemBCastSS << "\"cargo\": \"" << Telemetry::telemetry()->job->cargo << "\",";
	telemBCastSS << "\"mass\": " << Telemetry::telemetry()->job->mass << ",";
	telemBCastSS << "\"income\": " << Telemetry::telemetry()->job->income << ",";
	telemBCastSS << "\"destinationCityID\": \"" << Telemetry::telemetry()->job->destinationCityID << "\",";
	telemBCastSS << "\"destinationCity\": \"" << Telemetry::telemetry()->job->destinationCity << "\",";
	telemBCastSS << "\"destinationCompanyID\": \"" << Telemetry::telemetry()->job->destinationCompanyID << "\",";
	telemBCastSS << "\"destinationCompany\": \"" << Telemetry::telemetry()->job->destinationCompany << "\",";
	telemBCastSS << "\"sourceCityID\": \"" << Telemetry::telemetry()->job->sourceCityID << "\",";
	telemBCastSS << "\"sourceCity\": \"" << Telemetry::telemetry()->job->sourceCity << "\",";
	telemBCastSS << "\"sourceCompanyID\": \"" << Telemetry::telemetry()->job->sourceCompanyID << "\",";
	telemBCastSS << "\"sourceCompany\": \"" << Telemetry::telemetry()->job->sourceCompany << "\",";
	telemBCastSS << "\"deliveryTime\": " << Telemetry::telemetry()->job->deliveryTime << ",";
	telemBCastSS << "\"isLate\": " << boolToString(Telemetry::telemetry()->job->isLate) << ",";
	telemBCastSS << "\"timeRemaining\": " << Telemetry::telemetry()->job->timeRemaining << ",";
	telemBCastSS << "\"onJob\": " << boolToString(Telemetry::telemetry()->job->onJob) << "";
	telemBCastSS << "},";


	telemBCastSS << "\"trailer\": {";
	telemBCastSS << "\"linearVelocity\": { ";
	telemBCastSS << "\"x\": " << Telemetry::telemetry()->trailer->linearVelocity.GetX() << ",";
	telemBCastSS << "\"y\": " << Telemetry::telemetry()->trailer->linearVelocity.GetY() << ",";
	telemBCastSS << "\"z\": " << Telemetry::telemetry()->trailer->linearVelocity.GetZ() << "";
	telemBCastSS << "},";
	telemBCastSS << "\"angularVelocity\": { ";
	telemBCastSS << "\"x\": " << Telemetry::telemetry()->trailer->angularVelocity.GetX() << ",";
	telemBCastSS << "\"y\": " << Telemetry::telemetry()->trailer->angularVelocity.GetY() << ",";
	telemBCastSS << "\"z\": " << Telemetry::telemetry()->trailer->angularVelocity.GetZ() << "";
	telemBCastSS << "},";
	telemBCastSS << "\"linearAcceleration\": { ";
	telemBCastSS << "\"x\": " << Telemetry::telemetry()->trailer->linearAcceleration.GetX() << ",";
	telemBCastSS << "\"y\": " << Telemetry::telemetry()->trailer->linearAcceleration.GetY() << ",";
	telemBCastSS << "\"z\": " << Telemetry::telemetry()->trailer->linearAcceleration.GetZ() << "";
	telemBCastSS << "},";
	telemBCastSS << "\"angularAcceleration\": { ";
	telemBCastSS << "\"x\": " << Telemetry::telemetry()->trailer->angularAcceleration.GetX() << ",";
	telemBCastSS << "\"y\": " << Telemetry::telemetry()->trailer->angularAcceleration.GetY() << ",";
	telemBCastSS << "\"z\": " << Telemetry::telemetry()->trailer->angularAcceleration.GetZ() << "";
	telemBCastSS << "},";




	//FOR EACH WHEEL
	if(Telemetry::telemetry()->trailer->wheelCountLock.try_lock() && Telemetry::telemetry()->game->substanceLock.try_lock())
	{
		telemBCastSS << "\"wheelCount\": " << Telemetry::telemetry()->trailer->wheelCount << ",";
		telemBCastSS << "\"wheelInfo\": [" << "";
		for(unsigned int a = 0; a < Telemetry::telemetry()->trailer->wheelCount; a++)
		{


			telemBCastSS << "{";
			telemBCastSS << "\"suspensionDeflection\": " << Telemetry::telemetry()->trailer->wheelSuspensionDeflections[a] << ",";
			telemBCastSS << "\"onGround\": " << boolToString(Telemetry::telemetry()->trailer->wheelOnGround[a]) << ",";
			try {
				telemBCastSS << "\"substance\": \"" << Telemetry::telemetry()->game->substances.at(Telemetry::telemetry()->trailer->wheelSubstance[a]) << "\",";
			}
			catch(std::exception ex)
			{
				telemBCastSS << "\"substance\": \"" << "static" << "\",";
			}
			telemBCastSS << "\"angularVelocity\": " << Telemetry::telemetry()->trailer->wheelAngularVelocity[a] << ",";
			telemBCastSS << "\"position\": {" << "\"x\": " << Telemetry::telemetry()->trailer->wheelPosition[a].GetX() << ", \"y\": " << Telemetry::telemetry()->trailer->wheelPosition[a].GetY() << ", \"z\": " << Telemetry::telemetry()->trailer->wheelPosition[a].GetZ() << " },";
			telemBCastSS << "\"steerable\": " << boolToString(Telemetry::telemetry()->trailer->wheelSteerable[a]) << ",";
			telemBCastSS << "\"simulated\": " << boolToString(Telemetry::telemetry()->trailer->wheelSimulated[a]) << ",";
			telemBCastSS << "\"radius\": " << Telemetry::telemetry()->trailer->wheelRadius[a] << ",";
			telemBCastSS << "\"steering\": " << Telemetry::telemetry()->trailer->wheelSteering[a] << ",";
			telemBCastSS << "\"rotation\": " << Telemetry::telemetry()->trailer->wheelRotation[a] << ",";
			telemBCastSS << "\"powered\": " << boolToString(Telemetry::telemetry()->trailer->wheelPowered[a]) << "";
			telemBCastSS << "}";

			if(a < Telemetry::telemetry()->trailer->wheelCount-1)
			{
				telemBCastSS << ",";
			}
			else
			{
				telemBCastSS << "";
			}
		}
		telemBCastSS << "],";
		Telemetry::telemetry()->trailer->wheelCountLock.unlock();
		Telemetry::telemetry()->game->substanceLock.unlock();
	}
	else
	{
		telemBCastSS << "\"wheelCount\": " << MAX_WHEEL_COUNT << ",";

		telemBCastSS << "\"wheelInfo\": [" << "";
		for(int a = 0; a < MAX_WHEEL_COUNT; a++)
		{
			telemBCastSS << "{";
			telemBCastSS << "\"suspensionDeflection\": " << 0.00f << ",";
			telemBCastSS << "\"onGround\": " << boolToString(false) << ",";
			telemBCastSS << "\"substance\": \"" << "static" << "\",";
			
			telemBCastSS << "\"angularVelocity\": " << 0.00f << ",";

			telemBCastSS << "\"position\": {" << "\"x\": " << 0.00f << ", \"y\": " << 0.00f << ", \"z\": " << 0.00f << " },";
			telemBCastSS << "\"steerable\": " << boolToString(false) << ",";
			telemBCastSS << "\"simulated\": " << boolToString(false) << ",";
			telemBCastSS << "\"radius\": " << 0.00f << ",";
			telemBCastSS << "\"steering\": " << 0.00f << ",";
			telemBCastSS << "\"rotation\": " << 0.00f << ",";
			telemBCastSS << "\"powered\": " << boolToString(false) << "";
			telemBCastSS << "}";

			if(a < MAX_WHEEL_COUNT-1)
			{
				telemBCastSS << ",";
			}
			else
			{
				telemBCastSS << "";
			}
		}
		telemBCastSS << "],";
	}


	telemBCastSS << "\"id\": \"" << Telemetry::telemetry()->trailer->id << "\",";
	telemBCastSS << "\"cargoAccessoryId\": \"" << Telemetry::telemetry()->trailer->cargoAccessoryId << "\",";
	telemBCastSS << "\"hookPosition\": { \"x\": " << Telemetry::telemetry()->trailer->hookPosition.GetX() << ", \"y\": " << Telemetry::telemetry()->trailer->hookPosition.GetY() << ", \"z\": " << Telemetry::telemetry()->trailer->hookPosition.GetZ() << " }";

	telemBCastSS << "},";


	telemBCastSS << "\"user\": {";
	telemBCastSS << "\"steamID\": \"" << Job::Current()->steamID << "\",";
	telemBCastSS << "\"steamUsername\": \"" << Job::Current()->steamUsername << "\",";
	telemBCastSS << "\"DLC\": " << Job::Current()->steamDLCJSON << "";
	telemBCastSS << "}";

	telemBCastSS << "},";
	telemBCastSS << "\"jobData\": {";
	telemBCastSS << "\"status\": " << Job::Current()->status << ",";
	telemBCastSS << "\"wasSpeeding\": " << boolToString(Job::Current()->wasSpeeding) << ",";
	telemBCastSS << "\"jobStartedEventFired\": " << boolToString(Job::Current()->jobStartedEventFired) << ",";
	telemBCastSS << "\"isMultiplayer\": " << boolToString(Telemetry::telemetry()->game->isMultiplayer) << ",";
	telemBCastSS << "\"late\": " << boolToString(Job::Current()->late) << ",";
	telemBCastSS << "\"onJob\": " << boolToString(Job::Current()->onJob) << ",";
	telemBCastSS << "\"wasFinished\": " << boolToString(Job::Current()->wasFinished) << ",";
	telemBCastSS << "\"wasTrailerDisconnected\": " << boolToString(Job::Current()->wasTrailerDisconnnected) << ",";
	telemBCastSS << "\"sourceCity\": \"" << Job::Current()->sourceCity << "\",";
	telemBCastSS << "\"sourceCompany\": \"" << Job::Current()->sourceCompany << "\",";
	telemBCastSS << "\"destinationCity\": \"" << Job::Current()->destinationCity << "\",";
	telemBCastSS << "\"destinationCompany\": \"" << Job::Current()->destinationCompany << "\",";
	telemBCastSS << "\"cargo\": \"" << Job::Current()->cargo << "\",";
	telemBCastSS << "\"truckMake\": \"" << Job::Current()->truckMake << "\",";
	telemBCastSS << "\"truckModel\": \"" << Job::Current()->truckModel << "\",";
	telemBCastSS << "\"game\": \"" << Job::Current()->game << "\",";
	telemBCastSS << "\"sourceCityID\": \"" << Job::Current()->sourceCityID << "\",";
	telemBCastSS << "\"sourceCompanyID\": \"" << Job::Current()->sourceCompanyID << "\",";
	telemBCastSS << "\"destinationCityID\": \"" << Job::Current()->destinationCityID << "\",";
	telemBCastSS << "\"destinationCompanyID\": \"" << Job::Current()->destinationCompanyID << "\",";
	telemBCastSS << "\"cargoID\": \"" << Job::Current()->cargoID << "\",";
	telemBCastSS << "\"truckMakeID\": \"" << Job::Current()->truckMakeID << "\",";
	telemBCastSS << "\"truckModelID\": \"" << Job::Current()->truckModelID << "\",";
	telemBCastSS << "\"gameID\": \"" << Job::Current()->gameID << "\",";
	telemBCastSS << "\"gameVersion\": \"" << Job::Current()->gameVersion << "\",";
	telemBCastSS << "\"pluginVersion\": \"" << Job::Current()->pluginVersion << "\",";
	telemBCastSS << "\"topSpeed\": " << Job::Current()->topSpeed << ",";
	telemBCastSS << "\"speedingCount\": " << Job::Current()->speedingCount << ",";
	telemBCastSS << "\"income\": " << Job::Current()->income << ",";
	telemBCastSS << "\"trailerMass\": " << Job::Current()->trailerMass << ",";
	telemBCastSS << "\"distanceDriven\": " << Job::Current()->distanceDriven << ",";
	telemBCastSS << "\"fuelBurned\": " << Job::Current()->fuelBurned << ",";
	telemBCastSS << "\"fuelPurchased\": " << Job::Current()->fuelPurchased << ",";
	telemBCastSS << "\"startOdometer\": " << Job::Current()->startOdometer << ",";
	telemBCastSS << "\"endOdometer\": " << Job::Current()->endOdometer << ",";
	telemBCastSS << "\"timeRemaining\": " << Job::Current()->timeRemaining << ",";
	telemBCastSS << "\"timeStarted\": " << Job::Current()->timeStarted << ",";
	telemBCastSS << "\"timeDue\": " << Job::Current()->timeDue << ",";
	telemBCastSS << "\"timeDelivered\": " << Job::Current()->timeDelivered << ",";
	telemBCastSS << "\"collisionCount\": " << Job::Current()->collisionCount << ",";
	telemBCastSS << "\"finishTrailerDamage\": " << Job::Current()->finishTrailerDamage << ",";
	telemBCastSS << "\"startTrailerDamage\": " << Job::Current()->startTrailerDamage << ",";
	telemBCastSS << "\"deliveryX\": " << Job::Current()->deliveryX << ",";
	telemBCastSS << "\"deliveryY\": " << Job::Current()->deliveryY << ",";
	telemBCastSS << "\"deliveryZ\": " << Job::Current()->deliveryZ << ",";
	telemBCastSS << "\"pickupX\": " << Job::Current()->pickupX << ",";
	telemBCastSS << "\"pickupY\": " << Job::Current()->pickupY << ",";
	telemBCastSS << "\"pickupZ\": " << Job::Current()->pickupZ << ",";
	telemBCastSS << "\"trailerDeliveryX\": " << Job::Current()->trailerDeliveryX << ",";
	telemBCastSS << "\"trailerDeliveryY\": " << Job::Current()->trailerDeliveryY << ",";
	telemBCastSS << "\"trailerDeliveryZ\": " << Job::Current()->trailerDeliveryZ << ",";
	telemBCastSS << "\"trailerPickupX\": " << Job::Current()->trailerPickupX << ",";
	telemBCastSS << "\"trailerPickupY\": " << Job::Current()->trailerPickupY << ",";
	telemBCastSS << "\"trailerPickupZ\": " << Job::Current()->trailerPickupZ << ",";
	telemBCastSS << "\"startEngineDamage\": " << Job::Current()->startEngineDamage << ",";
	telemBCastSS << "\"startTransmissionDamage\": " << Job::Current()->startTransmissionDamage << ",";
	telemBCastSS << "\"startCabinDamage\": " << Job::Current()->startCabinDamage << ",";
	telemBCastSS << "\"startChassisDamage\": " << Job::Current()->startChassisDamage << ",";
	telemBCastSS << "\"startWheelDamage\": " << Job::Current()->startWheelDamage << ",";
	telemBCastSS << "\"finishEngineDamage\": " << Job::Current()->finishEngineDamage << ",";
	telemBCastSS << "\"finishTransmissionDamage\": " << Job::Current()->finishTransmissionDamage << ",";
	telemBCastSS << "\"finishCabinDamage\": " << Job::Current()->finishCabinDamage << ",";
	telemBCastSS << "\"finishChassisDamage\": " << Job::Current()->finishChassisDamage << ",";
	telemBCastSS << "\"finishWheelDamage\": " << Job::Current()->finishWheelDamage << ",";
	telemBCastSS << "\"totalEngineDamage\": " << Job::Current()->totalEngineDamage << ",";
	telemBCastSS << "\"totalTransmissionDamage\": " << Job::Current()->totalTransmissionDamage << ",";
	telemBCastSS << "\"totalCabinDamage\": " << Job::Current()->totalCabinDamage << ",";
	telemBCastSS << "\"totalChassisDamage\": " << Job::Current()->totalChassisDamage << ",";
	telemBCastSS << "\"totalWheelDamage\": " << Job::Current()->totalWheelDamage << ",";
	telemBCastSS << "\"totalTrailerDamage\": " << Job::Current()->totalTrailerDamage << ",";
	telemBCastSS << "\"navigationDistanceRemaining\": " << Job::Current()->navigationDistanceRemaining << ",";
	telemBCastSS << "\"fuel\": " << Job::Current()->fuel << ",";
	telemBCastSS << "\"odometer\": " << Job::Current()->odometer << ",";
	telemBCastSS << "\"engineDamage\": " << Job::Current()->engineDamage << ",";
	telemBCastSS << "\"transmissionDamage\": " << Job::Current()->transmissionDamage << ",";
	telemBCastSS << "\"cabinDamage\": " << Job::Current()->cabinDamage << ",";
	telemBCastSS << "\"chassisDamage\": " << Job::Current()->chassisDamage << ",";
	telemBCastSS << "\"wheelDamage\": " << Job::Current()->wheelDamage << ",";
	telemBCastSS << "\"trailerDamage\": " << Job::Current()->trailerDamage << ",";
	telemBCastSS << "\"realTimeStarted\": " << Job::Current()->realTimeStarted << ",";
	telemBCastSS << "\"realTimeEnded\": " << Job::Current()->realTimeEnded << ",";
	telemBCastSS << "\"realTimeTaken\": " << Job::Current()->realTimeTaken << "";
	
	telemBCastSS << "}";
	telemBCastSS << "}";
	telemBCastSS << "}\r";
	telemJSON = telemBCastSS.str();
	telemBCastSS.clear();
	telemBCastSS.str("");




	if (strcmp(status, "JOB FINISHED") == 0)
	{
		Debugger::log_line(std::string("[  Job Finished  ] " + telemJSON).c_str());
	}
	return telemJSON;
}


void TelemetryWorking()
{
	while (shuttingDown == false)
	{
		if (shuttingDown == true)
			return;
		milliseconds_from_epoch = getEpoch();
		if (milliseconds_from_epoch - lastTelemBCastTime >= 1000)
		{
			lastTelemBCastTime = milliseconds_from_epoch;
			if(readyToStart != false)
				SendStatusMessage("TELEMETRY");
		}
#ifdef _WIN32
		Sleep(1);
#endif
#if defined(__linux__) || defined(__APPLE__)
		sleep(1);
#endif
	}
}
void SendStatusMessage(const char* status,bool ignoreShutdown)
{
	if(!shuttingDown || ignoreShutdown != false) {
		std::ostringstream os;
		std::string sendStr;
		try {

			os << generateTelemetryJSON(status);
			sendStr = os.str();
			os.str("");
			os.clear();
			if (findSuffix(sendStr, endOfJSON))
				s->BroadcastMessage(sendStr);
			else
				Debugger::log_line("FAILED JSON");
		}
		catch (std::exception &e)
		{
			Debugger::log_line(e.what());
		}
		sendStr = "";
	}
}

bool findSuffix(std::string &str, std::string &pattern)
{
	const char* haystace = str.c_str();
	const char* need = pattern.c_str();

	int pos = (int)strlen(haystace) - 1;
	for (int c = (int)strlen(need) - 1; c > -1; c--)
	{
		if (pattern.at(c) != str.at(pos))
			return false;
		pos--;
	}

	return true;
}
void SendSpeedMessage(float speed, float limit, float x, float y, float z, float tx, float ty, float tz)
{
	std::string telemJSON;
	std::stringstream telemBCastSS;
	telemBCastSS << "{";
	telemBCastSS << "\"data\": {";
	telemBCastSS << "\"status\": \"SPEEDING\",";
	telemBCastSS << "\"offense\": {";
	telemBCastSS << "\"reason\": \"speeding\",";
	telemBCastSS << "\"speed\": " << speed << ",";
	telemBCastSS << "\"speedLimit\": " << limit << ",";
	telemBCastSS << "\"x\": " << x << ",";
	telemBCastSS << "\"y\": " << y << ",";
	telemBCastSS << "\"z\": " << z << ",";
	telemBCastSS << "\"trailerX\": " << tx << ",";
	telemBCastSS << "\"trailerY\": " << ty << ",";
	telemBCastSS << "\"trailerZ\": " << tz << "";
	telemBCastSS << "}";
	telemBCastSS << "}";
	telemBCastSS << "}\r";
	telemJSON = telemBCastSS.str();
	telemBCastSS.clear();
	telemBCastSS.str("");
	s->BroadcastMessage(telemJSON);
}
void SendCollisionMessage(float speed, float limit, float x, float y, float z, float cabinDamage, float chassisDamage, float engineDamage, float transmissionDamage, float tx, float ty, float tz, float trailerDamage)
{
	std::string telemJSON;
	std::stringstream telemBCastSS;
	telemBCastSS << "{";
	telemBCastSS << "\"data\": {";
	telemBCastSS << "\"status\": \"COLLISION\",";
	telemBCastSS << "\"offense\": {";
	telemBCastSS << "\"reason\": \"collision\",";
	telemBCastSS << "\"speed\": " << speed << ",";
	telemBCastSS << "\"speedLimit\": " << limit << ",";
	telemBCastSS << "\"x\": " << x << ",";
	telemBCastSS << "\"y\": " << y << ",";
	telemBCastSS << "\"z\": " << z << ",";
	telemBCastSS << "\"trailerX\": " << tx << ",";
	telemBCastSS << "\"trailerY\": " << ty << ",";
	telemBCastSS << "\"trailerZ\": " << tz << ",";

	telemBCastSS << "\"cabinDamage\": " << cabinDamage << ",";
	telemBCastSS << "\"chassisDamage\": " << chassisDamage << ",";
	telemBCastSS << "\"engineDamage\": " << engineDamage << ",";
	telemBCastSS << "\"transmissionDamage\": " << transmissionDamage << ",";
	telemBCastSS << "\"trailerDamage\": " << trailerDamage << "";

	telemBCastSS << "}";
	telemBCastSS << "}";
	telemBCastSS << "}\r";
	telemJSON = telemBCastSS.str();
	telemBCastSS.clear();
	telemBCastSS.str("");
	s->BroadcastMessage(telemJSON);
}
void SendTransportationMessage()
{

}
void SendLateFeeMessage(unsigned int timeDue, unsigned int time, float x, float y, float z, float tx, float ty, float tz, float fee)
{
	std::string telemJSON;
	std::stringstream telemBCastSS;
	telemBCastSS << "{";
	telemBCastSS << "\"data\": {";
	telemBCastSS << "\"status\": \"LATE\",";
	telemBCastSS << "\"offense\": {";
	telemBCastSS << "\"reason\": \"late\",";
	telemBCastSS << "\"timeDue\": " << timeDue << ",";
	telemBCastSS << "\"currentGameTime\": " << time << ",";
	telemBCastSS << "\"x\": " << x << ",";
	telemBCastSS << "\"y\": " << y << ",";
	telemBCastSS << "\"z\": " << z << ",";
	telemBCastSS << "\"trailerX\": " << tx << ",";
	telemBCastSS << "\"trailerY\": " << ty << ",";
	telemBCastSS << "\"trailerZ\": " << tz << ",";
	telemBCastSS << "\"fee\": " << fee << "";
	telemBCastSS << "}";
	telemBCastSS << "}";
	telemBCastSS << "}\r";
	telemJSON = telemBCastSS.str();
	telemBCastSS.clear();
	telemBCastSS.str("");
	s->BroadcastMessage(telemJSON);
}
void SendTollMessage()
{

}




long long getEpoch()
{
	return (boost::posix_time::microsec_clock::local_time() - time_epoch).total_microseconds()/1000;
}
std::string convertToDayTime(unsigned int minutes)
{
	struct tm gameTimeTM;

	gameTimeTM = { 0 };
	gameTimeTM.tm_hour = 0; gameTimeTM.tm_sec = 0;
	gameTimeTM.tm_year = 101; gameTimeTM.tm_mon = 0; gameTimeTM.tm_mday = 1;
	gameTimeTM.tm_min = minutes;

    mktime(&gameTimeTM);
    char myTime[16] = "";
    strftime(myTime, 15, "%a %R", &gameTimeTM);

    return std::string(myTime);
    
}
std::string convertToDateTime(unsigned int minutes)
{
	struct tm gameTimeTM;

	gameTimeTM = { 0 };
	gameTimeTM.tm_hour = 0; gameTimeTM.tm_sec = 0;
	gameTimeTM.tm_year = 101; gameTimeTM.tm_mon = 0; gameTimeTM.tm_mday = 1;
	gameTimeTM.tm_min = minutes;

    mktime(&gameTimeTM);
    char myTime[26] = "";
    strftime(myTime, 25, "%Y-%m-%d %H:%M", &gameTimeTM);

    return std::string(myTime);
}
void doSaveETCARSFile()
{
	std::string telemJSON;
	std::stringstream telemSaveSS;
	telemSaveSS << "{";
	telemSaveSS << "\"data\": {";
	telemSaveSS << "\"ETCARSVersion\": {" << "\"minorVersion\":" << Telemetry::telemetry()->pluginVersion->minorVersion << ", \"majorVersion\": " << Telemetry::telemetry()->pluginVersion->majorVersion << "},";

	telemSaveSS << "\"last\": {" << "\"lastOdometer\": " << shutdownJob.lastOdometer << ", \"lastFuel\": " << shutdownJob.lastFuel << "},";

	telemSaveSS << "\"job\": {";
	
	telemSaveSS << "\"steamID\": " << "\"" << shutdownJob.steamID << "\",";
	telemSaveSS << "\"topSpeed\": " << shutdownJob.topSpeed << ",";
	telemSaveSS << "\"wasSpeeding\": " << boolToString(shutdownJob.wasSpeeding) << ",";
	telemSaveSS << "\"speedingCount\": " << shutdownJob.speedingCount << ",";
	telemSaveSS << "\"sourceCityID\": " << "\"" << shutdownJob.sourceCityID << "\",";
	telemSaveSS << "\"sourceCompanyID\": " << "\"" << shutdownJob.sourceCompanyID << "\",";
	telemSaveSS << "\"destinationCityID\": " << "\"" << shutdownJob.destinationCityID << "\",";
	telemSaveSS << "\"destinationCompanyID\": " << "\"" << shutdownJob.destinationCompanyID << "\",";
	telemSaveSS << "\"truckMakeID\": " << "\"" << shutdownJob.truckMakeID << "\",";
	telemSaveSS << "\"truckModelID\": " << "\"" << shutdownJob.truckModelID << "\",";
	telemSaveSS << "\"cargoID\": " << "\"" << shutdownJob.cargoID << "\",";
	telemSaveSS << "\"income\": " << shutdownJob.income << ",";
	telemSaveSS << "\"mass\": " << shutdownJob.trailerMass << ",";
	telemSaveSS << "\"distanceDriven\": " << shutdownJob.distanceDriven << ",";
	telemSaveSS << "\"fuelBurned\": " << shutdownJob.fuelBurned << ",";
	telemSaveSS << "\"fuelPurchased\": " << shutdownJob.fuelPurchased << ",";
	telemSaveSS << "\"startOdometer\": " << shutdownJob.startOdometer << ",";
	telemSaveSS << "\"collisionCount\": " << shutdownJob.collisionCount << ",";
	telemSaveSS << "\"startTrailerDamage\": " << shutdownJob.startTrailerDamage << ",";
	telemSaveSS << "\"startEngineDamage\": " << shutdownJob.startEngineDamage << ",";
	telemSaveSS << "\"startTransmissionDamage\": " << shutdownJob.startTransmissionDamage << ",";
	telemSaveSS << "\"startCabinDamage\": " << shutdownJob.startCabinDamage << ",";
	telemSaveSS << "\"startChassisDamage\": " << shutdownJob.startChassisDamage << ",";
	telemSaveSS << "\"startWheelDamage\": " << shutdownJob.startWheelDamage << ",";
	telemSaveSS << "\"trailerPickupX\": " << shutdownJob.trailerPickupX << ",";
	telemSaveSS << "\"trailerPickupY\": " << shutdownJob.trailerPickupY << ",";
	telemSaveSS << "\"trailerPickupZ\": " << shutdownJob.trailerPickupZ << ",";
	telemSaveSS << "\"pickupX\": " << shutdownJob.pickupX << ",";
	telemSaveSS << "\"pickupY\": " << shutdownJob.pickupY << ",";
	telemSaveSS << "\"pickupZ\": " << shutdownJob.pickupZ << ",";
	telemSaveSS << "\"totalTrailerDamage\": " << shutdownJob.totalTrailerDamage << ",";
	telemSaveSS << "\"totalEngineDamage\": " << shutdownJob.totalEngineDamage << ",";
	telemSaveSS << "\"totalTransmissionDamage\": " << shutdownJob.totalTransmissionDamage << ",";
	telemSaveSS << "\"totalCabinDamage\": " << shutdownJob.totalCabinDamage << ",";
	telemSaveSS << "\"totalChassisDamage\": " << shutdownJob.totalChassisDamage << ",";
	telemSaveSS << "\"totalWheelDamage\": " << shutdownJob.totalWheelDamage << ",";
	telemSaveSS << "\"trailerDamage\": " << shutdownJob.totalTrailerDamage << ",";
	telemSaveSS << "\"engineDamage\": " << shutdownJob.totalEngineDamage << ",";
	telemSaveSS << "\"transmissionDamage\": " << shutdownJob.totalTransmissionDamage << ",";
	telemSaveSS << "\"cabinDamage\": " << shutdownJob.totalCabinDamage << ",";
	telemSaveSS << "\"chassisDamage\": " << shutdownJob.totalChassisDamage << ",";
	telemSaveSS << "\"wheelDamage\": " << shutdownJob.totalWheelDamage << ",";
	telemSaveSS << "\"realTimeStarted\": " << shutdownJob.realTimeStarted << ",";
	telemSaveSS << "\"timeStarted\": " << shutdownJob.timeStarted << ",";
	telemSaveSS << "\"cargoAccessoryID\": " << "\"" << shutdownJob.cargoAccessoryID << "\",";
	telemSaveSS << "\"realTimeTaken\": " << shutdownJob.realTimeTaken << ",";
	telemSaveSS << "\"transportLocations\": [";
	telemSaveSS << "]";

	telemSaveSS << "},";


	telemSaveSS << "\"previousJob\": {";
	telemSaveSS << "\"steamID\": " << "\"" << previousJob.steamID << "\",";
	telemSaveSS << "\"sourceCityID\": " << "\"" << previousJob.sourceCityID << "\",";
	telemSaveSS << "\"sourceCompanyID\": " << "\"" << previousJob.sourceCompanyID << "\",";
	telemSaveSS << "\"destinationCityID\": " << "\"" << previousJob.destinationCityID << "\",";
	telemSaveSS << "\"destinationCompanyID\": " << "\"" << previousJob.destinationCompanyID << "\",";
	telemSaveSS << "\"truckMakeID\": " << "\"" << previousJob.truckMakeID << "\",";
	telemSaveSS << "\"truckModelID\": " << "\"" << previousJob.truckModelID << "\",";
	telemSaveSS << "\"cargoID\": " << "\"" << previousJob.cargoID << "\",";
	telemSaveSS << "\"income\": " << previousJob.income << ",";
	telemSaveSS << "\"trailerMass\": " << previousJob.trailerMass << ",";
	telemSaveSS << "\"cargoAccessoryID\": " << "\"" << previousJob.cargoAccessoryID << "\"";
	telemSaveSS << "}";
	
	telemSaveSS << "}";//data
	telemSaveSS << "}";//root
	telemJSON = telemSaveSS.str();
	telemSaveSS.clear();
	telemSaveSS.str("");
	const char* extra = Telemetry::telemetry()->game->isMultiplayer ? "mp" : "";
	NewMessage::SaveNewMessage(telemJSON.c_str(), Job::Current()->steamID,Job::Current()->steamUsername,Job::Current()->gameID,Telemetry::telemetry()->game->architecture.c_str(),Telemetry::telemetry()->game->osEnvironment.c_str(),Telemetry::telemetry()->game->gameName.c_str(), extra);

}
void doLoadETCARSFile()
{
	if (!hasLoadedFile)
	{
		const char* extra = Telemetry::telemetry()->game->isMultiplayer ? "mp" : "";
		std::string TelemJSON = NewMessage::ReadNewMessage(Job::Current()->steamID, Job::Current()->steamUsername, Job::Current()->gameID, Telemetry::telemetry()->game->architecture.c_str(), Telemetry::telemetry()->game->osEnvironment.c_str(), Telemetry::telemetry()->game->gameName.c_str(), extra);
		if (!TelemJSON.empty())
		{
			try
			{
				rapidjson::Document doc;
				
				TelemJSON = TelemJSON.substr(0, TelemJSON.find_last_of('}', 0) - 2);
				doc.Parse(TelemJSON.c_str());
				Debugger::log_line(TelemJSON.c_str());
				if (!doc.HasParseError())
				{
					if (doc.IsObject())
					{
						Debugger::log_line("Loading Save....");

						
						if (doc["data"]["ETCARSVersion"]["minorVersion"].GetInt() >= Telemetry::telemetry()->pluginVersion->minorVersion && doc["data"]["ETCARSVersion"]["majorVersion"].GetInt() >= Telemetry::telemetry()->pluginVersion->majorVersion)
						{
							//versions match, let's load it up!

							//check if telemetry reports a job
							if (Telemetry::telemetry()->job->onJob)
							{
								//check odometer and fuel....see if they match
								if (abs(doc["data"]["last"]["lastOdometer"].GetFloat() - Telemetry::telemetry()->truck->odometer) >= 15.00 && abs(doc["data"]["last"]["lastFuel"].GetFloat() - Telemetry::telemetry()->truck->fuel->currentLitres) >= 15.00)
								{
									//check if steam id match...
									if (!static_cast<std::string>(doc["data"]["job"]["steamID"].GetString()).empty())
									{
										if (
											strcmp(static_cast<std::string>(doc["data"]["job"]["steamID"].GetString()).c_str(), shutdownJob.steamID.c_str()) == 0
											&& strcmp(static_cast<std::string>(doc["data"]["job"]["truckMakeID"].GetString()).c_str(), shutdownJob.truckMakeID) == 0
											&& strcmp(static_cast<std::string>(doc["data"]["job"]["truckModelID"].GetString()).c_str(), shutdownJob.truckModelID) == 0
											&& strcmp(static_cast<std::string>(doc["data"]["job"]["cargoAccessoryID"].GetString()).c_str(), Telemetry::telemetry()->trailer->cargoAccessoryId.c_str()) == 0
											&& strcmp(static_cast<std::string>(doc["data"]["job"]["cargoID"].GetString()).c_str(), shutdownJob.cargoID) == 0
											&& strcmp(static_cast<std::string>(doc["data"]["job"]["destinationCityID"].GetString()).c_str(), shutdownJob.destinationCityID) == 0
											&& strcmp(static_cast<std::string>(doc["data"]["job"]["destinationCompanyID"].GetString()).c_str(), shutdownJob.destinationCompanyID) == 0
											&& strcmp(static_cast<std::string>(doc["data"]["job"]["sourceCityID"].GetString()).c_str(), shutdownJob.sourceCityID) == 0
											&& strcmp(static_cast<std::string>(doc["data"]["job"]["sourceCompanyID"].GetString()).c_str(), shutdownJob.sourceCompanyID) == 0
											&& doc["data"]["job"]["mass"].GetFloat() == shutdownJob.trailerMass
											&& doc["data"]["job"]["income"].GetFloat() == shutdownJob.income
											)
										{

											bool previousMatches = false;
											//check if we have a previous job...
											if (!static_cast<std::string>(doc["data"]["previousJob"]["steamID"].GetString()).empty())
											{
												previousMatches = strcmp(static_cast<std::string>(doc["data"]["previousJob"]["steamID"].GetString()).c_str(), shutdownJob.steamID.c_str()) == 0
													&& strcmp(static_cast<std::string>(doc["data"]["previousJob"]["truckMakeID"].GetString()).c_str(), shutdownJob.truckMakeID) == 0
													&& strcmp(static_cast<std::string>(doc["data"]["previousJob"]["truckModelID"].GetString()).c_str(), shutdownJob.truckModelID) == 0
													&& strcmp(static_cast<std::string>(doc["data"]["previousJob"]["cargoAccessoryID"].GetString()).c_str(), Telemetry::telemetry()->trailer->cargoAccessoryId.c_str()) == 0
													&& strcmp(static_cast<std::string>(doc["data"]["previousJob"]["cargoID"].GetString()).c_str(), shutdownJob.cargoID) == 0
													&& strcmp(static_cast<std::string>(doc["data"]["previousJob"]["destinationCityID"].GetString()).c_str(), shutdownJob.destinationCityID) == 0
													&& strcmp(static_cast<std::string>(doc["data"]["previousJob"]["destinationCompanyID"].GetString()).c_str(), shutdownJob.destinationCompanyID) == 0
													&& strcmp(static_cast<std::string>(doc["data"]["previousJob"]["sourceCityID"].GetString()).c_str(), shutdownJob.sourceCityID) == 0
													&& strcmp(static_cast<std::string>(doc["data"]["previousJob"]["sourceCompanyID"].GetString()).c_str(), shutdownJob.sourceCompanyID) == 0
													&& doc["data"]["previousJob"]["trailerMass"].GetFloat() == shutdownJob.trailerMass
													&& doc["data"]["previousJob"]["income"].GetFloat() == shutdownJob.income;

												previousJob.steamID = shutdownJob.steamID.c_str();

												previousJob.steamID = shutdownJob.steamID;
												previousJob.truckMakeID = shutdownJob.truckMakeID;
												previousJob.truckModelID = shutdownJob.truckModelID;
												previousJob.cargoAccessoryID = shutdownJob.cargoAccessoryID;
												previousJob.cargoID = shutdownJob.cargoID;
												previousJob.destinationCityID = shutdownJob.destinationCityID;
												previousJob.destinationCompanyID = shutdownJob.destinationCompanyID;
												previousJob.sourceCityID = shutdownJob.sourceCityID;
												previousJob.sourceCompanyID = shutdownJob.sourceCompanyID;
												previousJob.trailerMass = shutdownJob.trailerMass;
												previousJob.income = shutdownJob.income;

											}
											
											
											if (!previousMatches)
											{
												Job::Current()->loadedFromSave = true;

												Job::Current()->topSpeed = doc["data"]["job"]["topSpeed"].GetFloat();
												Job::Current()->wasSpeeding = doc["data"]["job"]["wasSpeeding"].GetBool();
												Job::Current()->speedingCount = doc["data"]["job"]["speedingCount"].GetInt();
												Job::Current()->distanceDriven = doc["data"]["job"]["distanceDriven"].GetFloat();
												Job::Current()->fuelBurned = doc["data"]["job"]["fuelBurned"].GetFloat();
												Job::Current()->fuelPurchased = doc["data"]["job"]["fuelPurchased"].GetFloat();
												Job::Current()->startOdometer = doc["data"]["job"]["startOdometer"].GetFloat();
												Job::Current()->collisionCount = doc["data"]["job"]["collisionCount"].GetInt();
												Job::Current()->startTrailerDamage = doc["data"]["job"]["startTrailerDamage"].GetFloat();
												Job::Current()->startEngineDamage = doc["data"]["job"]["startEngineDamage"].GetFloat();
												Job::Current()->startTransmissionDamage = doc["data"]["job"]["startTransmissionDamage"].GetFloat();
												Job::Current()->startCabinDamage = doc["data"]["job"]["startCabinDamage"].GetFloat();
												Job::Current()->startChassisDamage = doc["data"]["job"]["startChassisDamage"].GetFloat();
												Job::Current()->startWheelDamage = doc["data"]["job"]["startWheelDamage"].GetFloat();
												Job::Current()->trailerPickupX = doc["data"]["job"]["trailerPickupX"].GetFloat();
												Job::Current()->trailerPickupY = doc["data"]["job"]["trailerPickupY"].GetFloat();
												Job::Current()->trailerPickupZ = doc["data"]["job"]["trailerPickupZ"].GetFloat();
												Job::Current()->pickupX = doc["data"]["job"]["pickupX"].GetFloat();
												Job::Current()->pickupY = doc["data"]["job"]["pickupY"].GetFloat();
												Job::Current()->pickupZ = doc["data"]["job"]["pickupZ"].GetFloat();
												Job::Current()->totalTrailerDamage = doc["data"]["job"]["totalTrailerDamage"].GetFloat();
												Job::Current()->totalEngineDamage = doc["data"]["job"]["totalEngineDamage"].GetFloat();
												Job::Current()->totalTransmissionDamage = doc["data"]["job"]["totalTransmissionDamage"].GetFloat();
												Job::Current()->totalCabinDamage = doc["data"]["job"]["totalCabinDamage"].GetFloat();
												Job::Current()->totalChassisDamage = doc["data"]["job"]["totalChassisDamage"].GetFloat();
												Job::Current()->totalWheelDamage = doc["data"]["job"]["totalWheelDamage"].GetFloat();
												Job::Current()->realTimeStarted = doc["data"]["job"]["realTimeStarted"].GetInt64();
												Job::Current()->timeStarted = doc["data"]["job"]["timeStarted"].GetInt();
												Job::Current()->realTimeTaken = doc["data"]["job"]["realTimeTaken"].GetInt64();
												shutdownJob.topSpeed = doc["data"]["job"]["topSpeed"].GetFloat();
												shutdownJob.wasSpeeding = doc["data"]["job"]["wasSpeeding"].GetBool();
												shutdownJob.speedingCount = doc["data"]["job"]["speedingCount"].GetInt();
												shutdownJob.distanceDriven = doc["data"]["job"]["distanceDriven"].GetFloat();
												shutdownJob.fuelBurned = doc["data"]["job"]["fuelBurned"].GetFloat();
												shutdownJob.fuelPurchased = doc["data"]["job"]["fuelPurchased"].GetFloat();
												shutdownJob.startOdometer = doc["data"]["job"]["startOdometer"].GetFloat();
												shutdownJob.collisionCount = doc["data"]["job"]["collisionCount"].GetInt();
												shutdownJob.startTrailerDamage = doc["data"]["job"]["startTrailerDamage"].GetFloat();
												shutdownJob.startEngineDamage = doc["data"]["job"]["startEngineDamage"].GetFloat();
												shutdownJob.startTransmissionDamage = doc["data"]["job"]["startTransmissionDamage"].GetFloat();
												shutdownJob.startCabinDamage = doc["data"]["job"]["startCabinDamage"].GetFloat();
												shutdownJob.startChassisDamage = doc["data"]["job"]["startChassisDamage"].GetFloat();
												shutdownJob.startWheelDamage = doc["data"]["job"]["startWheelDamage"].GetFloat();
												shutdownJob.trailerPickupX = doc["data"]["job"]["trailerPickupX"].GetFloat();
												shutdownJob.trailerPickupY = doc["data"]["job"]["trailerPickupY"].GetFloat();
												shutdownJob.trailerPickupZ = doc["data"]["job"]["trailerPickupZ"].GetFloat();
												shutdownJob.pickupX = doc["data"]["job"]["pickupX"].GetFloat();
												shutdownJob.pickupY = doc["data"]["job"]["pickupY"].GetFloat();
												shutdownJob.pickupZ = doc["data"]["job"]["pickupZ"].GetFloat();
												shutdownJob.totalTrailerDamage = doc["data"]["job"]["totalTrailerDamage"].GetFloat();
												shutdownJob.totalEngineDamage = doc["data"]["job"]["totalEngineDamage"].GetFloat();
												shutdownJob.totalTransmissionDamage = doc["data"]["job"]["totalTransmissionDamage"].GetFloat();
												shutdownJob.totalCabinDamage = doc["data"]["job"]["totalCabinDamage"].GetFloat();
												shutdownJob.totalChassisDamage = doc["data"]["job"]["totalChassisDamage"].GetFloat();
												shutdownJob.totalWheelDamage = doc["data"]["job"]["totalWheelDamage"].GetFloat();
												shutdownJob.realTimeStarted = doc["data"]["job"]["realTimeStarted"].GetInt64();
												shutdownJob.timeStarted = doc["data"]["job"]["timeStarted"].GetInt();
												shutdownJob.realTimeTaken = doc["data"]["job"]["realTimeTaken"].GetInt64();
												shutdownJob.trailerDamage = doc["data"]["job"]["trailerDamage"].GetFloat();
												shutdownJob.engineDamage = doc["data"]["job"]["engineDamage"].GetFloat();
												shutdownJob.transmissionDamage = doc["data"]["job"]["transmissionDamage"].GetFloat();
												shutdownJob.cabinDamage = doc["data"]["job"]["cabinDamage"].GetFloat();
												shutdownJob.chassisDamage = doc["data"]["job"]["chassisDamage"].GetFloat();
												shutdownJob.wheelDamage = doc["data"]["job"]["wheelDamage"].GetFloat();

												Job::Current()->trailerDamage = shutdownJob.trailerDamage;
												Job::Current()->engineDamage = shutdownJob.engineDamage;
												Job::Current()->transmissionDamage = shutdownJob.transmissionDamage;
												Job::Current()->cabinDamage = shutdownJob.cabinDamage;
												Job::Current()->chassisDamage = shutdownJob.chassisDamage;
												Job::Current()->wheelDamage = shutdownJob.wheelDamage;

												//Telemetry::telemetry()->job->transportLocations

												hasLoadedFile = true;
												Debugger::log_line("Save loading completed");
												
												return;
											}
										}
										else
										{
											if (!strcmp(static_cast<std::string>(doc["data"]["job"]["steamID"].GetString()).c_str(), Job::Current()->steamID) == 0)
											{
												Debugger::log_line("SteamID no match");
												Debugger::log_line(std::string("JSON VALUE: " + static_cast<std::string>(doc["data"]["job"]["steamID"].GetString()) + " | " + "CURRENT: " + Job::Current()->steamID).c_str());
											}

											if(!strcmp(static_cast<std::string>(doc["data"]["job"]["truckMakeID"].GetString()).c_str(), Job::Current()->truckMakeID) == 0)
											{
												Debugger::log_line("truckMakeID no match");
												Debugger::log_line(std::string("JSON VALUE: " + static_cast<std::string>(doc["data"]["job"]["truckMakeID"].GetString()) + " | " + "CURRENT: " + Job::Current()->truckMakeID).c_str());
											}

											if (!strcmp(static_cast<std::string>(doc["data"]["job"]["truckModelID"].GetString()).c_str(), Job::Current()->truckModelID) == 0)
											{
												Debugger::log_line("truckModelID no match");
												Debugger::log_line(std::string("JSON VALUE: " + static_cast<std::string>(doc["data"]["job"]["truckModelID"].GetString()) + " | " + "CURRENT: " + Job::Current()->truckModelID).c_str());
											}

											if (!strcmp(static_cast<std::string>(doc["data"]["job"]["cargoAccessoryID"].GetString()).c_str(), Telemetry::telemetry()->trailer->cargoAccessoryId.c_str()) == 0)
											{
												Debugger::log_line("cargoAccessoryID no match");
												Debugger::log_line(std::string("JSON VALUE: " + static_cast<std::string>(doc["data"]["job"]["cargoAccessoryID"].GetString()) + " | " + "CURRENT: " + Telemetry::telemetry()->trailer->cargoAccessoryId).c_str());
											}

											if (!strcmp(static_cast<std::string>(doc["data"]["job"]["cargoID"].GetString()).c_str(), Job::Current()->cargoID) == 0)
											{
												Debugger::log_line("cargoID no match");
												Debugger::log_line(std::string("JSON VALUE: " + static_cast<std::string>(doc["data"]["job"]["cargoID"].GetString()) + " | " + "CURRENT: " + Job::Current()->cargoID).c_str());
											}

											if (!strcmp(static_cast<std::string>(doc["data"]["job"]["destinationCityID"].GetString()).c_str(), Job::Current()->destinationCityID) == 0)
											{
												Debugger::log_line("destinationCityID no match");
												Debugger::log_line(std::string("JSON VALUE: " + static_cast<std::string>(doc["data"]["job"]["destinationCityID"].GetString()) + " | " + "CURRENT: " + Job::Current()->destinationCityID).c_str());
											}

											if (!strcmp(static_cast<std::string>(doc["data"]["job"]["destinationCompanyID"].GetString()).c_str(), Job::Current()->destinationCompanyID) == 0)
											{
												Debugger::log_line("destinationCompanyID no match");
												Debugger::log_line(std::string("JSON VALUE: " + static_cast<std::string>(doc["data"]["job"]["destinationCompanyID"].GetString()) + " | " + "CURRENT: " + Job::Current()->destinationCompanyID).c_str());
											}

											if (!strcmp(static_cast<std::string>(doc["data"]["job"]["sourceCityID"].GetString()).c_str(), Job::Current()->sourceCityID) == 0)
											{
												Debugger::log_line("sourceCityID no match");
												Debugger::log_line(std::string("JSON VALUE: " + static_cast<std::string>(doc["data"]["job"]["sourceCityID"].GetString()) + " | " + "CURRENT: " + Job::Current()->sourceCityID).c_str());
											}

											if (!strcmp(static_cast<std::string>(doc["data"]["job"]["sourceCompanyID"].GetString()).c_str(), Job::Current()->sourceCompanyID) == 0)
											{
												Debugger::log_line("sourceCompanyID no match");
												Debugger::log_line(std::string("JSON VALUE: " + static_cast<std::string>(doc["data"]["job"]["sourceCompanyID"].GetString()) + " | " + "CURRENT: " + Job::Current()->sourceCompanyID).c_str());
											}

											if (doc["data"]["job"]["mass"].GetFloat() != Job::Current()->trailerMass)
											{
												Debugger::log_line("mass no match");
											}

											if (doc["data"]["job"]["income"].GetUint() != Job::Current()->income)
											{
												Debugger::log_line("income no match");
											}
										}
									}
								}
							}
							//No else statement needed, this IF statement ensures it's the same job
							Debugger::log_line("Save loading completed - job save mismatch/no job/matches previous - reset");
							shutdownJob.reset();
						}
						else
						{
							previousJob.reset();
							shutdownJob.reset();
							Debugger::log_line("Save loading completed - version mismatch - reset");
						}
					}
				}
				else
				{
					std::string err = rapidjson::GetParseError_En(doc.GetParseError());
					Debugger::log_line(rapidjson::GetParseError_En(doc.GetParseError()));
					Debugger::log_line(TelemJSON.c_str());
				}
			}
			catch (std::exception ex)
			{
				Debugger::log_line("Exception loading save: ");
				Debugger::log_line(ex.what());
				doSaveETCARSFile();
			}
		}
		hasLoadedFile = true;
	}
}
void UploadCompletedDelivery()
{
	std::map<std::string, std::string> data = std::map<std::string, std::string>();
	std::string telemJSON;
	std::stringstream telemBCastSS;

	
	telemBCastSS << "{";
	telemBCastSS << "\"jobData\": {";
	telemBCastSS << "\"isMultiplayer\": " << boolToString(Telemetry::telemetry()->game->isMultiplayer) << ",";
	telemBCastSS << "\"late\": " << boolToString(Job::Current()->late) << ",";
	telemBCastSS << "\"sourceCity\": \"" << Job::Current()->sourceCity << "\",";
	telemBCastSS << "\"sourceCompany\": \"" << Job::Current()->sourceCompany << "\",";
	telemBCastSS << "\"destinationCity\": \"" << Job::Current()->destinationCity << "\",";
	telemBCastSS << "\"destinationCompany\": \"" << Job::Current()->destinationCompany << "\",";
	telemBCastSS << "\"cargo\": \"" << Job::Current()->cargo << "\",";
	telemBCastSS << "\"truckMake\": \"" << Job::Current()->truckMake << "\",";
	telemBCastSS << "\"truckModel\": \"" << Job::Current()->truckModel << "\",";
	telemBCastSS << "\"game\": \"" << Job::Current()->game << "\",";
	telemBCastSS << "\"sourceCityID\": \"" << Job::Current()->sourceCityID << "\",";
	telemBCastSS << "\"sourceCompanyID\": \"" << Job::Current()->sourceCompanyID << "\",";
	telemBCastSS << "\"destinationCityID\": \"" << Job::Current()->destinationCityID << "\",";
	telemBCastSS << "\"destinationCompanyID\": \"" << Job::Current()->destinationCompanyID << "\",";
	telemBCastSS << "\"cargoID\": \"" << Job::Current()->cargoID << "\",";
	telemBCastSS << "\"truckMakeID\": \"" << Job::Current()->truckMakeID << "\",";
	telemBCastSS << "\"truckModelID\": \"" << Job::Current()->truckModelID << "\",";
	telemBCastSS << "\"gameID\": \"" << Job::Current()->gameID << "\",";
	telemBCastSS << "\"gameVersion\": \"" << Job::Current()->gameVersion << "\",";
	telemBCastSS << "\"pluginVersion\": \"" << Job::Current()->pluginVersion << "\",";
	telemBCastSS << "\"income\": " << Job::Current()->income << ",";
	telemBCastSS << "\"trailerMass\": " << Job::Current()->trailerMass << ",";
	telemBCastSS << "\"distanceDriven\": " << Job::Current()->distanceDriven << ",";
	telemBCastSS << "\"fuelBurned\": " << Job::Current()->fuelBurned << ",";
	telemBCastSS << "\"fuelPurchased\": " << Job::Current()->fuelPurchased << ",";
	telemBCastSS << "\"startOdometer\": " << Job::Current()->startOdometer << ",";
	telemBCastSS << "\"endOdometer\": " << Job::Current()->endOdometer << ",";
	telemBCastSS << "\"collisionCount\": " << Job::Current()->collisionCount << ",";
	telemBCastSS << "\"finishTrailerDamage\": " << Job::Current()->finishTrailerDamage << ",";
	telemBCastSS << "\"startTrailerDamage\": " << Job::Current()->startTrailerDamage << ",";
	telemBCastSS << "\"deliveryX\": " << Job::Current()->deliveryX << ",";
	telemBCastSS << "\"deliveryY\": " << Job::Current()->deliveryY << ",";
	telemBCastSS << "\"deliveryZ\": " << Job::Current()->deliveryZ << ",";
	telemBCastSS << "\"pickupX\": " << Job::Current()->pickupX << ",";
	telemBCastSS << "\"pickupY\": " << Job::Current()->pickupY << ",";
	telemBCastSS << "\"pickupZ\": " << Job::Current()->pickupZ << ",";
	telemBCastSS << "\"trailerDeliveryX\": " << Job::Current()->trailerDeliveryX << ",";
	telemBCastSS << "\"trailerDeliveryY\": " << Job::Current()->trailerDeliveryY << ",";
	telemBCastSS << "\"trailerDeliveryZ\": " << Job::Current()->trailerDeliveryZ << ",";
	telemBCastSS << "\"trailerPickupX\": " << Job::Current()->trailerPickupX << ",";
	telemBCastSS << "\"trailerPickupY\": " << Job::Current()->trailerPickupY << ",";
	telemBCastSS << "\"trailerPickupZ\": " << Job::Current()->trailerPickupZ << ",";
	telemBCastSS << "\"startEngineDamage\": " << Job::Current()->startEngineDamage << ",";
	telemBCastSS << "\"startTransmissionDamage\": " << Job::Current()->startTransmissionDamage << ",";
	telemBCastSS << "\"startCabinDamage\": " << Job::Current()->startCabinDamage << ",";
	telemBCastSS << "\"startChassisDamage\": " << Job::Current()->startChassisDamage << ",";
	telemBCastSS << "\"startWheelDamage\": " << Job::Current()->startWheelDamage << ",";
	telemBCastSS << "\"finishEngineDamage\": " << Job::Current()->finishEngineDamage << ",";
	telemBCastSS << "\"finishTransmissionDamage\": " << Job::Current()->finishTransmissionDamage << ",";
	telemBCastSS << "\"finishCabinDamage\": " << Job::Current()->finishCabinDamage << ",";
	telemBCastSS << "\"finishChassisDamage\": " << Job::Current()->finishChassisDamage << ",";
	telemBCastSS << "\"finishWheelDamage\": " << Job::Current()->finishWheelDamage << ",";
	telemBCastSS << "\"totalEngineDamage\": " << Job::Current()->totalEngineDamage << ",";
	telemBCastSS << "\"totalTransmissionDamage\": " << Job::Current()->totalTransmissionDamage << ",";
	telemBCastSS << "\"totalCabinDamage\": " << Job::Current()->totalCabinDamage << ",";
	telemBCastSS << "\"totalChassisDamage\": " << Job::Current()->totalChassisDamage << ",";
	telemBCastSS << "\"totalWheelDamage\": " << Job::Current()->totalWheelDamage << ",";
	telemBCastSS << "\"totalTrailerDamage\": " << Job::Current()->totalTrailerDamage << ",";
	telemBCastSS << "\"osEnvironment\": \"" << Telemetry::telemetry()->game->osEnvironment << "\",";
	telemBCastSS << "\"architecture\": \"" << Telemetry::telemetry()->game->architecture << "\",";
	telemBCastSS << "\"steamID\": \"" << Job::Current()->steamID << "\",";
	telemBCastSS << "\"steamUsername\": \"" << Job::Current()->steamUsername << "\",";
	telemBCastSS << "\"navigationDistanceRemaining\": " << Job::Current()->navigationDistanceRemaining;

	telemBCastSS << "}";
	telemBCastSS << "}\r";
	telemJSON = telemBCastSS.str();


	telemBCastSS.clear();
	telemBCastSS.str("");

	
	data["data"] = NewMessage::prepToUpload(telemJSON);
	telemJSON = "";

	std::string res = api->post("https://etcarsdev.menzelstudios.com/api/v1/uploadDelivery", data);
	Debugger::log_line("DELIVERY UPLOADED!");
	Debugger::log_line(res.c_str());
	
	
	data.clear();
	res = "";
	
}
