#include "Debugger.h"



void Debugger::init(scs_log_t log, const char* logFileName)
{
	if (Debugger::log_file)
		return;
	Debugger::game_log = log;
#ifdef _WIN32
	fopen_s(&Debugger::log_file, logFileName, "wt");
#endif
#if defined(__linux__) || defined(__APPLE__)
	Debugger::log_file = fopen(logFileName, "wt");
#endif
	Debugger::log(LogTo::BOTH, LogLevel::INFO, "[ Debugger ]   Log opened or re-initialized.");
}

void Debugger::log(LogTo log, LogLevel level, const char* message)
{
	_logLine = "[ ";
	_logLine += currentTime();
	_logLine += " ]  [ ETCARS Plugin ] [ ";
	_logLine += logLevelToString(level);
	_logLine += " ]   ";
	_logLine += message;

	if ((int(log) == int(LogTo::GAME) || int(log) == int(LogTo::BOTH) && Debugger::game_log != NULL))
		Debugger::game_log(SCS_LOG_TYPE_message, _logLine.c_str());
	if ((int(log) == int(LogTo::FILE) || int(log) == int(LogTo::BOTH)) && Debugger::log_file)
	{
		fprintf(Debugger::log_file, _logLine.c_str());
		fprintf(Debugger::log_file, "\n");
	}
}
void Debugger::log_line(const char* message)
{
	Debugger::log(LogTo::BOTH, LogLevel::DBG, message);
}
void Debugger::finish_log()
{
	if (!log_file)
		return;
	Debugger::log(LogTo::FILE, LogLevel::INFO, "[ Debugger ]   Log Closing.");
	fclose(Debugger::log_file);
	Debugger::game_log = NULL;
	Debugger::log_file = NULL;
}
const char* Debugger::currentTime()
{
	if (!Debugger::timeNBuffer)
		Debugger::timeNBuffer = "";
	memset(Debugger::timeBuffer, 0, sizeof Debugger::timeBuffer);
	memset(Debugger::timeNBuffer, 0, std::strlen(Debugger::timeNBuffer));
	Debugger::now = time(0);
#ifdef _WIN32
	localtime_s(&Debugger::localTime, &Debugger::now);
	strftime(Debugger::timeBuffer, 4096, "%a %b %d %R:%S %Y", &Debugger::localTime);
	Debugger::timeNBuffer = new char[strlen(reinterpret_cast<const char*>(Debugger::timeBuffer)) + 1];
	strncpy_s(Debugger::timeNBuffer, strlen(reinterpret_cast<const char*>(Debugger::timeBuffer)) + 1, reinterpret_cast<const char*>(Debugger::timeBuffer), strlen(reinterpret_cast<const char*>(Debugger::timeBuffer)) + 1);
#endif
#if defined(__linux__) || defined(__APPLE__)
	Debugger::localTime = *localtime(&Debugger::now);
	strftime(Debugger::timeBuffer, 4096, "%a %b %d %R:%S %Y", &Debugger::localTime);
	Debugger::timeNBuffer = new char[strlen(reinterpret_cast<const char*>(Debugger::timeBuffer)) + 1];
	strncpy(Debugger::timeNBuffer, reinterpret_cast<const char*>(Debugger::timeBuffer), strlen(reinterpret_cast<const char*>(Debugger::timeBuffer)) + 1);
#endif
	
	
	
	return Debugger::timeNBuffer;
}
const char * Debugger::logLevelToString(LogLevel level)
{
	switch (level)
	{
	case LogLevel::DBG:
	{
		return "DEBUG";
	}
	case LogLevel::INFO:
	{
		return "INFO";
	}
	case LogLevel::WARNING:
	{
		return "WARNING";
	}
	case LogLevel::FATAL:
	{
		return "FATAL";
	}
	default:
		return "UNKNOWN";
	}
}