#include "Job.h"
Job::Job()
{
	this->wasSpeeding = false;
	this->speedingCount = 0;
	this->steamID = "";
	this->steamUsername = "";
	this->steamDLCJSON = "";
	this->status = 0;
	this->loadedFromSave = false;
	this->sourceCity = "";
	this->sourceCompany = "";
	this->destinationCity = "";
	this->destinationCompany = "";
	this->cargo = "";
	this->truckMake = "";
	this->truckModel = "";
	this->game = "";
	this->sourceCityID = "";
	this->sourceCompanyID = "";
	this->destinationCityID = "";
	this->destinationCompanyID = "";
	this->cargoID = "";
	this->truckMakeID = "";
	this->truckModelID = "";
	this->gameID = "";
	this->gameVersion = "";

	this->topSpeed = 0.00;
	this->income = 0;
	this->trailerMass = 0.00;
	this->distanceDriven = 0.00;
	this->fuelBurned = 0.00;
	this->fuelPurchased = 0.00;
	this->startOdometer = 0.00;
	this->endOdometer = 0.00;
	this->timeRemaining = 0;
	this->timeStarted = 0;
	this->timeDue = 0;

	this->finishTrailerDamage = 0.00;
	this->startTrailerDamage = 0.00;

	this->deliveryX = 0.00;
	this->deliveryY = 0.00;
	this->deliveryZ = 0.00;

	this->pickupX = 0.00;
	this->pickupY = 0.00;
	this->pickupZ = 0.00;

	this->trailerDeliveryX = 0.00;
	this->trailerDeliveryY = 0.00;
	this->trailerDeliveryZ = 0.00;

	this->trailerPickupX = 0.00;
	this->trailerPickupY = 0.00;
	this->trailerPickupZ = 0.00;

	this->startEngineDamage = 0.00;
	this->startTransmissionDamage = 0.00;
	this->startCabinDamage = 0.00;
	this->startChassisDamage = 0.00;
	this->startWheelDamage = 0.00;

	this->finishEngineDamage = 0.00;
	this->finishTransmissionDamage = 0.00;
	this->finishCabinDamage = 0.00;
	this->finishChassisDamage = 0.00;
	this->finishWheelDamage = 0.00;
	this->isMultiplayer = false;
	this->collisionCount = 0;
	this->late = false;
	this->timeDelivered = 0;
	this->wasTrailerDisconnnected = false;
	this->totalCabinDamage = 0.00;
	this->totalChassisDamage = 0.00;
	this->totalEngineDamage = 0.00;
	this->totalTransmissionDamage = 0.00;
	this->totalWheelDamage = 0.00;
	this->totalTrailerDamage = 0.00;
	this->navigationDistanceRemaining = 0.00;
	this->engineDamage = 0.00;
	this->transmissionDamage = 0.00;
	this->cabinDamage = 0.00;
	this->chassisDamage = 0.00;
	this->wheelDamage = 0.00;
	this->trailerDamage = 0.00;
	this->realTimeEnded = 0;
	this->realTimeStarted = 0;
	this->realTimeTaken = 0;
	this->jobStartedEventFired = false;
	//LIVE PARAMETERS REQUIRED FOR MATHS
	this->fuel = 0.00;
	this->odometer = 0.00;


}
Job::~Job() {}

void Job::Reset()
{
	this->wasSpeeding = false;
	//this->loadedFromSave = false;
	this->speedingCount = 0;
	this->status = 0;
	this->sourceCity = "";
	this->sourceCompany = "";
	this->destinationCity = "";
	this->destinationCompany = "";
	this->cargo = "";
	this->truckMake = "";
	this->truckModel = "";
	this->sourceCityID = "";
	this->sourceCompanyID = "";
	this->destinationCityID = "";
	this->destinationCompanyID = "";
	this->cargoID = "";
	this->truckMakeID = "";
	this->truckModelID = "";
	this->wasFinished = false;

	this->topSpeed = 0.00;
	this->income = 0;
	this->trailerMass = 0.00;
	this->distanceDriven = 0.00;
	this->fuelBurned = 0.00;
	this->fuelPurchased = 0.00;
	this->startOdometer = 0.00;
	this->endOdometer = 0.00;
	this->timeRemaining = 0;
	this->timeStarted = 0;
	this->timeDue = 0;

	this->finishTrailerDamage = 0.00;
	this->startTrailerDamage = 0.00;

	this->deliveryX = 0.00;
	this->deliveryY = 0.00;
	this->deliveryZ = 0.00;

	this->pickupX = 0.00;
	this->pickupY = 0.00;
	this->pickupZ = 0.00;

	this->trailerDeliveryX = 0.00;
	this->trailerDeliveryY = 0.00;
	this->trailerDeliveryZ = 0.00;

	this->trailerPickupX = 0.00;
	this->trailerPickupY = 0.00;
	this->trailerPickupZ = 0.00;

	this->startEngineDamage = 0.00;
	this->startTransmissionDamage = 0.00;
	this->startCabinDamage = 0.00;
	this->startChassisDamage = 0.00;
	this->startWheelDamage = 0.00;

	this->finishEngineDamage = 0.00;
	this->finishTransmissionDamage = 0.00;
	this->finishCabinDamage = 0.00;
	this->finishChassisDamage = 0.00;
	this->finishWheelDamage = 0.00;
	this->isMultiplayer = false;
	this->collisionCount = 0;
	this->late = false;
	this->timeDelivered = 0;
	this->wasTrailerDisconnnected = false;
	this->totalCabinDamage = 0.00;
	this->totalChassisDamage = 0.00;
	this->totalEngineDamage = 0.00;
	this->totalTransmissionDamage = 0.00;
	this->totalWheelDamage = 0.00;
	this->totalTrailerDamage = 0.00;
	this->navigationDistanceRemaining = 0.00;
	this->engineDamage = 0.00;
	this->transmissionDamage = 0.00;
	this->cabinDamage = 0.00;
	this->chassisDamage = 0.00;
	this->wheelDamage = 0.00;
	this->realTimeEnded = 0;
	this->realTimeStarted = 0;
	this->realTimeTaken = 0;
	this->jobStartedEventFired = false;
	//LIVE PARAMETERS REQUIRED FOR MATHS
	this->fuel = 0.00;
	this->odometer = 0.00;
	Debugger::log_line("JOB INFORMATION RESET");
}
Job* Job::Current(bool requireNew)
{
	if (!Job::current)
		Job::current = new Job();
	if (requireNew)
	{
		Job::current->Reset();
	}
	return Job::current;
}
Job* Job::current = 0;

StorageJob::StorageJob()
{
	this->wasSpeeding = false;
	this->speedingCount = 0;
	this->steamID = "";
	this->sourceCityID = "";
	this->sourceCompanyID = "";
	this->destinationCityID = "";
	this->destinationCompanyID = "";
	this->cargoID = "";
	this->truckMakeID = "";
	this->truckModelID = "";
	this->cargoAccessoryID = "";
	this->topSpeed = 0.00f;
	this->income = 0;
	this->trailerMass = 0.00f;
	this->distanceDriven = 0.00f;
	this->fuelBurned = 0.00f;
	this->fuelPurchased = 0.00f;
	this->startOdometer = 0.00f;
	this->collisionCount = 0;
	this->startTrailerDamage = 0.00f;
	this->pickupX = 0.00f;
	this->pickupY = 0.00f;
	this->pickupZ = 0.00f;
	this->trailerPickupX = 0.00f;
	this->trailerPickupY = 0.00f;
	this->trailerPickupZ = 0.00f;
	this->startEngineDamage = 0.00f;
	this->startTransmissionDamage = 0.00f;
	this->startCabinDamage = 0.00f;
	this->startChassisDamage = 0.00f;
	this->startWheelDamage = 0.00f;

	this->totalEngineDamage = 0.00f;
	this->totalTransmissionDamage = 0.00f;
	this->totalCabinDamage = 0.00f;
	this->totalChassisDamage = 0.00f;
	this->totalWheelDamage = 0.00f;
	this->totalTrailerDamage = 0.00f;

	this->realTimeStarted = 0;
	this->realTimeTaken = 0;
	this->lastFuel = 0.00f;
	this->lastOdometer = 0.00f;
	this->ETCARSMajorVersion = 0;
	this->ETCARSMinorVersion = 0;
	this->timeStarted = 0;
	this->configLoaded = false;
}
void StorageJob::reset()
{
	this->timeStarted = 0;
	this->wasSpeeding = false;
	this->speedingCount = 0;
	this->steamID = "";
	this->sourceCityID = "";
	this->sourceCompanyID = "";
	this->destinationCityID = "";
	this->destinationCompanyID = "";
	this->cargoID = "";
	this->truckMakeID = "";
	this->truckModelID = "";
	this->cargoAccessoryID = "";
	this->topSpeed = 0.00f;
	this->income = 0;
	this->trailerMass = 0.00f;
	this->distanceDriven = 0.00f;
	this->fuelBurned = 0.00f;
	this->fuelPurchased = 0.00f;
	this->startOdometer = 0.00f;
	this->collisionCount = 0;
	this->startTrailerDamage = 0.00f;
	this->pickupX = 0.00f;
	this->pickupY = 0.00f;
	this->pickupZ = 0.00f;
	this->trailerPickupX = 0.00f;
	this->trailerPickupY = 0.00f;
	this->trailerPickupZ = 0.00f;
	this->startEngineDamage = 0.00f;
	this->startTransmissionDamage = 0.00f;
	this->startCabinDamage = 0.00f;
	this->startChassisDamage = 0.00f;
	this->startWheelDamage = 0.00f;

	this->totalEngineDamage = 0.00f;
	this->totalTransmissionDamage = 0.00f;
	this->totalCabinDamage = 0.00f;
	this->totalChassisDamage = 0.00f;
	this->totalWheelDamage = 0.00f;
	this->totalTrailerDamage = 0.00f;

	this->realTimeStarted = 0;
	this->realTimeTaken = 0;
	this->lastFuel = 0.00f;
	this->lastOdometer = 0.00f;
	this->ETCARSMajorVersion = 0;
	this->ETCARSMinorVersion = 0;
	this->transportLocs.clear();
	this->configLoaded = false;
}