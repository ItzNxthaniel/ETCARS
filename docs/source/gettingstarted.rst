.. 
###############
Getting Started
###############

*******
Windows
*******
To install ETCARS on Windows, there are some prerequisites to installing:

- Valid OS
- American Truck Simulator and/or Euro Truck Simulator 2 (Steam Linked)
- Microsoft Visual C++ Redistributable 2017

--------------------------
Supported Windows Versions
--------------------------
The following Windows Versions are supported:

- Windows 7 x64
- Windows 8 x64
- Windows 8.1 x64
- Windows 10 x64

-----------------------------------------
Microsoft Visual C++ Redistributable 2017
-----------------------------------------
The Microsoft Visual C++ Redistributable 2017 is a dependency for the plugin to function properly in the Windwos environment.
For 64-bit users, you'll need to install both the 32-bit(x86) and 64-bit(x64) version of the redistributable. They can be downloaded using the links below:

- `x86 <https://aka.ms/vs/15/release/VC_redist.x86.exe>`_
- `x64 <https://aka.ms/vs/15/release/VC_redist.x64.exe>`_

----------
Installing
----------
Once the Microsoft Visual C++ Redistributable 2017 has been installed, you can now proceed to installing ETCARS!
To install ETCARS, simply download one of the installers from the ETCARS website at `https://etcars.menzelstudios.com <https://etcars.menzelstudios.com>`_.
Then run the installer and boom! You're all set to go!

*****
Linux
*****
To install the latest version of ETCARS on linux, open a terminal window in a directory you have write access to.
Then download the installation script by pasting the following line into the terminal window, then press enter.
::
    wget https://gitlab.com/jammerxd/ETCARS/blob/master/ETCARS/install_etcars_linux.sh
The installation script will use wget to download 2 files. One is the shared object plugin, the other is a file used by cURL. To run the installation script, execute the following by copying and pasting the following line into the terminal window and pressing enter.
::
    bash install_etcars_linux.sh
Congratulations! ETCARS is now installed and will run when your game runs!

********
Mac OS X
********
To install on Mac OS X, you'll need to download the Mac version of ETCARS and place it in the plugins folder of the eurotrucks2 executable. This is found by finding "Euro Truck Simulator 2" but instead of launching, right-click and click on "Show Package Contents". 
Then go into Contents->MacOS and create a plugins folder if not already existing and place the ETCARS plugin file in the plugins folder. This will allow the game to load ETCARS. The last step is to download the curl certificate bundle: `cacert.pem <https://curl.haxx.se/ca/cacert.pem>`_ and place 
it in your Documents/ETCARS folder(creating it if it doesn't already exist). See additional information below for reasons why this needs to be done.


**********************
Additional Information
**********************
The current install script is set to install the 64-bit edition ONLY. 
| 
| NOTE: ETCARS uses a certificate bundle for host verification. The file ca-certificates.crt can be downlocaded from the cURL web site as `cacert.pem <https://curl.haxx.se/ca/cacert.pem>`_ and then renamed to ca-certificates.crt and placed in /etc/ssl/certs/. This is required for cURL and ETCARS to work correctly.

